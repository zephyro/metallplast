
$(".btn_modal").fancybox({
    'padding'    : 0
});


(function() {

    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.page').toggleClass('nav_open');
    });

}());


$('.btn_top').on('click touchstart', function(e){
    $('html,body').stop().animate({ scrollTop: $('#top').offset().top }, 150);
});


// Mini Cart
$('.cart_toggle').on('click touchstart', function(e){
    e.preventDefault();
    $('.mini_cart').toggleClass('open');
});

$('body').click(function (event) {

    if ($(event.target).closest(".header__cart").length === 0) {
        $(".mini_cart").removeClass('open');
    }
});


// Переключение типа покупателя
(function() {

    $('.purchaser_type label').on('click touchstart', function(){

        var point = '#' + $(this).find('input').val();

        $('.checkout').find('.purchaser_tab').removeClass('active');
        $('.checkout').find(point).addClass('active');
    });

}());


// Самовывоз или Доствка
(function() {

    $('.delivery_type label').on('click touchstart', function(){

        var point = '#' + $(this).find('input').val();

        $('.checkout').find('.delivery').removeClass('active');
        $('.checkout').find(point).addClass('active');
    });

}());


// Filter
(function() {

    $('.catalog__filter_title').on('click touchstart', function(e){
        e.preventDefault();

        $(this).toggleClass('open');
        $('.catalog__filter_content').toggleClass('open');
    });

}());


var cat = new Swiper('.cat_nav__slider', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    navigation: {
        nextEl: '.cat_nav__next',
        prevEl: '.cat_nav__prev',
    },
});


var about = new Swiper('.about_gallery__slider', {
    slidesPerView: 4,
    spaceBetween: 10,
    // init: false,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        1440: {
            slidesPerView: 3
        },
        992: {
            slidesPerView: 2
        },

        576: {
            slidesPerView: 1
        }
    }
});

var gallery = new Swiper('.product__gallery_slider', {
    slidesPerView: 4,
    spaceBetween: 10,
    loop: true,
    breakpoints: {
        1720: {
            slidesPerView: 3,
            spaceBetween: 4,
        },
        992: {
            slidesPerView: 2,
            spaceBetween: 9,
        },
        576: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    }
});




/// tmp

(function() {

    $('.goods__cart .btn').on('click touchstart', function(e){
        e.preventDefault();
        console.log('add_cart');
        $('.modal_add_open').trigger('click');
    });

}());

// Input number

$('.minus').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
});
$('.plus').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
});


