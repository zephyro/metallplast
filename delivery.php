<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page" id="top">

            <!-- Header -->
            <div class="header_mobile">
                <div class="container">
                    <div class="header_mobile__wrap">
                        <a class="header_mobile__phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                        <a class="header_mobile__email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header_mobile__slogan">Производим автомобильные номера с 1991 года</div>
                    <div class="header__row">
                        <div class="header__left">
                            <a class="header__toggle nav_toggle" href="#"></a>
                            <a class="header__logo" href="/">
                                <img src="img/logo.svg" class="img-fluid" alt="">
                            </a>
                            <div class="header__slogan">Производим автомобильные номера с 1991 года</div>
                        </div>
                        <nav class="header__nav">
                            <ul>
                                <li><a href="#">О Компании</a></li>
                                <li><a href="#">Аксессуары</a></li>
                                <li><a href="#">Доставка и оплата</a></li>
                                <li><a href="#">Для производителей номеров</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </nav>
                        <div class="header__right">

                            <div class="header__slogan_tablet">Производим автомобильные номера с 1991 года</div>

                            <ul class="header__contact">
                                <li>
                                    <a class="header__contact_phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                                </li>
                                <li>
                                    <a class="header__contact_email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                                </li>
                            </ul>

                            <div class="header__cart">
                                <span>2</span>
                                <i>
                                    <img src="img/icon__cart.svg" class="img-fluid" alt="">
                                </i>
                                <a href="#" class="header__cart_link cart_toggle"></a>
                                <div class="mini_cart">
                                    <div class="mini_cart__heading">
                                        <strong>Корзина</strong>
                                        <span>В Вашей корзине 2 товара</span>
                                    </div>
                                    <div class="mini_cart__close cart_toggle"></div>
                                    <div class="mini_cart__content">
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>1</strong>
                                                    </div>
                                                    <a href="#">Легковые грузовые и автобусы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image">
                                                    <img src="images/mini_cat__item_01.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>1 200</strong> ₽</li>
                                        </ul>
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>3</strong>
                                                    </div>
                                                    <a href="#">Дорожно-строительная техника, тракторы, прицепы и полуприцепы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image mini_cart__image_small">
                                                    <img src="images/mini_cat__item_02.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>900</strong> ₽</li>
                                        </ul>
                                    </div>
                                    <div class="mini_cart__footer">
                                        <a href="#" class="btn btn_sm btn_yellow">Перейти в корзину</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <ul class="header__breadcrumbs">
                            <li><a href="#">Интернет-магазин</a></li>
                            <li><a href="#">Гражданские</a></li>
                            <li><span>Легковые, грузовые и автобусы</span></li>
                        </ul>

                    </div>

                </div>
            </header>

            <nav class="nav_mobile">
                <span class="nav_mobile__close nav_toggle"></span>
                <div class="nav_mobile__logo">
                    <img src="img/logo.svg" class="img-fluid" alt="">
                </div>
                <ul>
                    <li><a href="#">Интренет магазин</a></li>
                    <li><a href="#">О Компании</a></li>
                    <li><a href="#">Аксессуары</a></li>
                    <li><a href="#">Доставка и оплата</a></li>
                    <li><a href="#">Для производителей номеров</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </nav>
            <div class="nav_mobile__layout nav_toggle"></div>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="main_row">

                        <div class="sidebar">
                            <div class="sidebar__nav">
                                <a href="#" class="catalog__type catalog__type_civil">Гражданские</a>
                                <a href="#" class="catalog__type catalog__type_mil">Военные</a>
                                <a href="#"class="catalog__type catalog__type_diplomatic">Дипломатические</a>
                                <a href="#" class="catalog__type catalog__type_police">МВД</a>
                                <a href="#" class="catalog__type catalog__type_other">Другие</a>
                            </div>
                        </div>

                        <div class="info">

                            <h2>Оплата товара</h2>

                            <div class="payment">
                                <div class="payment__col">
                                    <h3>Физическим лицам</h3>
                                    <p>Из-за особенностей производства и правил продажи регистрационных знаков, производство и доставка продукции осуществляется только после предоплаты.</p>
                                    <p>Оплата заказа осуществляется через платежную систему ПАО «СБЕРБАНК».</p>
                                    <p>К оплате принимаются банковские карты платежных систем:</p>
                                    <ul class="payment__system">
                                        <li>
                                            <img src="img/logo__visa.svg" class="img-fluid" alt="">
                                        </li>
                                        <li>
                                            <img src="img/logo__mc.svg" class="img-fluid" alt="">
                                        </li>
                                        <li>
                                            <img src="img/logo__mir.svg" class="img-fluid" alt="">
                                        </li>
                                    </ul>

                                </div>
                                <div class="payment__col">
                                    <h3>Юридическим лицам</h3>
                                    <p>Оплата товара юридическими лицами осуществляется по факту выставления счета. Просто оформите заказ на сайте, и наш менеджер свяжется с вами по вопросу формирования необходимых документов. </p>
                                </div>
                            </div>

                            <h3>Описание процесса передачи данных</h3>
                            <div class="mb_40">
                                <p>
                                    Для оплаты (ввода реквизитов Вашей карты) Вы будете перенаправлены на платежный шлюз ПАО СБЕРБАНК. Соединение с платежным шлюзом и передача информации осуществляется в защищенном режиме с использованием протокола шифрования SSL. В случае если Ваш банк поддерживает технологию безопасного проведения интернет-платежей Verified By Visa или MasterCard SecureCode для проведения платежа также может потребоваться ввод специального пароля. Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой персональной информации обеспечивается ПАО СБЕРБАНК. Введенная информация не будет предоставлена третьим лицам за исключением случаев, предусмотренных законодательством РФ. Проведение платежей по банковским картам осуществляется в строгом соответствии с требованиями платежных систем МИР, Visa Int. и MasterCard Europe Sprl.
                                </p>
                            </div>

                            <div class="info__divider"></div>

                            <h2>Доставка товара</h2>

                            <div class="delivery_box">
                                <div class="delivery_box__icon">
                                    <i>
                                        <img src="img/icon__courier.svg" alt="">
                                    </i>
                                    <span>Курьер</span>
                                </div>
                                <div class="delivery_box__text">
                                    <p>Для вашего удобства в нашем магазине реализована возможность курьерской доставки продукции по всей территории России.</p>
                                    <p>Доставка курьером осуществляется в будние дни с 9-00 до 18-00. <br/>В оговоренной день доставки курьер заранее свяжется с Вами для подтверждения и возможности принять товар.</p>
                                    <h4>Срок доставки:</h4>
                                    <ul>
                                        <li>Срок доставки курьером по Москве: 1-3 дня.</li>
                                        <li>Срок доставки курьером в Московскую область: 3-4 дня.</li>
                                        <li>Срок доставки курьером в Санкт-Петербург: 3-4 дня.</li>
                                        <li>Срок доставки в другие регионы РФ: определяется в ходе оформления заказа, после выбора города и ввода точного адреса доставки.</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="delivery_box">
                                <div class="delivery_box__icon">
                                    <i>
                                        <img src="img/icon__postomat.svg" alt="">
                                    </i>
                                    <span>Доставка<br/>до постомата PickPoint</span>
                                </div>
                                <div class="delivery_box__text">
                                    <p>Постамат – это автоматизированные ячейки для посылок, из которых Вы сами забираете купленные товары в удобном для Вас месте и в удобное время. Постаматы и пункты выдачи PickPoint расположены в крупных торговых центрах и супермаркетах в большинстве Российских городов: карта постаматов PickPoint.</p>
                                    <h4>Срок доставки:</h4>
                                    <ul class="mb_20">
                                        <li>Срок доставки курьером по Москве:  2-3 дня.</li>
                                        <li>Срок доставки курьером в Санкт-Петербург: 2-4 дня.</li>
                                        <li>Срок доставки в другие города РФ: от 3 до 10 дней (в зависимости от удаленности от Москвы) – точный срок рассчитывается при оформлении заказа после выбора необходимого постомата PickPoint на карте.</li>
                                    </ul>
                                    <h4>Время доставки:</h4>
                                    <ul class="mb_20">
                                        <li>В момент поступления заказа в точку выдачи на указанный Вами номер телефона придет SMS с индивидуальным кодом открытия ячейки.</li>
                                        <li>Вы можете забрать свой заказ в любое удобное время в часы работы постамата в течение 3 дней. Если Вы не успеваете забрать свой заказ в течение 3 дней, то можете бесплатно продлить срок его хранения еще на 2 дня. По истечении указанного срока данный заказ будет считаться невостребованным</li>
                                    </ul>
                                    <h4>Стоимость доставки:</h4>
                                    <ul class="mb_20">
                                        <li>Рассчитывается при оформлении заказа по тарифам Pick Point. От 175 рублей.</li>
                                    </ul>
                                    <h4>Условия хранения в Постомате:</h4>
                                    <ul>
                                        <li>Отправление хранится в ячейке в течение 3 дней. Если Вы не успеваете забрать свой заказ в течение 3 дней, то можете бесплатно продлить срок его хранения еще на 2 дня. По истечении указанного срока данный заказ считается невостребованным и будет аннулирован.</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="delivery_box">
                                <div class="delivery_box__icon">
                                    <i>
                                        <img src="img/icon__post.svg" alt="">
                                    </i>
                                    <span>Доставка<br/>Почтой России</span>
                                </div>
                                <div class="delivery_box__text">
                                    <p>Для вашего удобства наша компания осуществляет доставку продукции до любого отделения Почты России на территории РФ.</p>
                                    <p>Стоимость и срок доставки зависит от выбранного отделения Почты России, и рассчитывается в ходе оформления заказа, после ввода вашего индекса.</p>
                                    <p>Стоимость доставки устанавливается тарифным планом Почты России.</p>
                                    <p>Обращаем ваше внимание, что, при оформлении заказа, срок доставки до конкретного региона указывается на основании данных службы «Почта России», и может быть увеличен ею без уведомления.</p>
                                </div>
                            </div>

                            <div class="delivery_box">
                                <div class="delivery_box__icon">
                                    <i>
                                        <img src="img/icon__delivery.svg" alt="">
                                    </i>
                                    <span>Самовывоз</span>
                                </div>
                                <div class="delivery_box__text">
                                    <p>Для вашего удобства наша компания предоставляет возможность осуществить самовывоз оплаченного заказа. Самовывоз осуществляется после подтверждения нашим менеджером окончания производства товара.</p>
                                    <p>Адрес самовывоза: г. Москва, Локомотивный проезд,  дом 21, строение 3, эт 1 ком 28.</p>
                                    <p>Режим работы: С понедельника по пятницу с 8-30 до 17-00.</p>
                                </div>
                            </div>

                            <h2>Возврат денежных средств</h2>
                            <p>В случае несоответствия указанного в заказе государственного регистрационного знака прикрепленным документам, производится возврат денежных средств Заказчику. Возврат переведенных средств, производится на Ваш банковский счет в течение 5—30 рабочих дней (срок зависит от Банка, который выдал Вашу банковскую карту). </p>

                        </div>


                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
