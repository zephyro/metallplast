<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page" id="top">

            <!-- Header -->
            <div class="header_mobile">
                <div class="container">
                    <div class="header_mobile__wrap">
                        <a class="header_mobile__phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                        <a class="header_mobile__email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header_mobile__slogan">Производим автомобильные номера с 1991 года</div>
                    <div class="header__row">
                        <div class="header__left">
                            <a class="header__toggle nav_toggle" href="#"></a>
                            <a class="header__logo" href="/">
                                <img src="img/logo.svg" class="img-fluid" alt="">
                            </a>
                            <div class="header__slogan">Производим автомобильные номера с 1991 года</div>
                        </div>
                        <nav class="header__nav">
                            <ul>
                                <li><a href="#">О Компании</a></li>
                                <li><a href="#">Аксессуары</a></li>
                                <li><a href="#">Доставка и оплата</a></li>
                                <li><a href="#">Для производителей номеров</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </nav>
                        <div class="header__right">

                            <div class="header__slogan_tablet">Производим автомобильные номера с 1991 года</div>

                            <ul class="header__contact">
                                <li>
                                    <a class="header__contact_phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                                </li>
                                <li>
                                    <a class="header__contact_email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                                </li>
                            </ul>

                            <div class="header__cart">
                                <span>2</span>
                                <i>
                                    <img src="img/icon__cart.svg" class="img-fluid" alt="">
                                </i>
                                <a href="#" class="header__cart_link cart_toggle"></a>
                                <div class="mini_cart">
                                    <div class="mini_cart__heading">
                                        <strong>Корзина</strong>
                                        <span>В Вашей корзине 2 товара</span>
                                    </div>
                                    <div class="mini_cart__close cart_toggle"></div>
                                    <div class="mini_cart__content">
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>1</strong>
                                                    </div>
                                                    <a href="#">Легковые грузовые и автобусы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image">
                                                    <img src="images/mini_cat__item_01.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>1 200</strong> <i class="ruble">¤</i></li>
                                        </ul>
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>3</strong>
                                                    </div>
                                                    <a href="#">Дорожно-строительная техника, тракторы, прицепы и полуприцепы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image mini_cart__image_small">
                                                    <img src="images/mini_cat__item_02.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>900</strong> <i class="ruble">¤</i></li>
                                        </ul>
                                    </div>
                                    <div class="mini_cart__footer">
                                        <a href="#" class="btn btn_sm btn_yellow">Перейти в корзину</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <ul class="header__breadcrumbs">
                            <li><a href="#">Интернет-магазин</a></li>
                            <li><span>Корзина</span></li>
                        </ul>

                    </div>

                </div>
            </header>

            <nav class="nav_mobile">
                <span class="nav_mobile__close nav_toggle"></span>
                <div class="nav_mobile__logo">
                    <img src="img/logo.svg" class="img-fluid" alt="">
                </div>
                <ul>
                    <li><a href="#">Интренет магазин</a></li>
                    <li><a href="#">О Компании</a></li>
                    <li><a href="#">Аксессуары</a></li>
                    <li><a href="#">Доставка и оплата</a></li>
                    <li><a href="#">Для производителей номеров</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </nav>
            <div class="nav_mobile__layout nav_toggle"></div>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="checkout_heading">
                        <h1>Корзина</h1>
                        <p>В Вашей корзине 2 товара:</p>
                    </div>

                    <div class="checkout">

                        <div class="checkout__block">

                            <div class="cart">
                                <ul class="cart__row cart__row_heading">
                                    <li>Товар</li>
                                    <li>Количество</li>
                                    <li>Документы</li>
                                    <li>Цена</li>
                                    <li>Удалить</li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <div class="cart__heading">Товар</div>
                                        <div class="cart__source">

                                            <div class="cart__auto">
                                                <div class="cart__auto_icon">тип <strong>1</strong></div>
                                                <div class="cart__auto_text">Легковые, грузовые и автобусы</div>
                                            </div>

                                            <ul class="cart__params">
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1">
                                                        <span>с отверстиями</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1">
                                                        <span>с флагом</span>
                                                    </label>
                                                </li>
                                            </ul>

                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_1">
                                                        <div class="number__image">
                                                            <img src="img/number.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_1_1">
                                                            <input type="text" class="num_1a" name="num_1a" value="" placeholder="м090мм">
                                                        </div>
                                                        <div class="chars chars__type_1_2">
                                                            <input type="text" class="num_1b" name="num_1b" value="" placeholder="090">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="cart__btn">

                                                <button class="btn btn_reg" type="button">
                                                    <i><img src="img/icon__edit.svg" class="img-fluid" alt=""></i>
                                                    <span>Редактировать номер</span>
                                                </button>

                                                <button class="btn btn_white btn_del" type="button">
                                                    <span>Удалить из<br/>корзины</span>
                                                </button>

                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Количество</div>
                                        <div class="cart__source">

                                            <ul class="cart__amount">
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1" checked>
                                                        <span>2 шт.</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1">
                                                        <span>1 шт.</span>
                                                    </label>
                                                </li>
                                            </ul>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Документы</div>
                                        <div class="cart__source">
                                            <h4>Свидететельство о регистрации ТС или ПТС*</h4>
                                            <ul class="cart__docs">
                                                <li>
                                                    <label class="cart__upload">
                                                        <input type="file" name="file">
                                                        <strong>1 сторона</strong>
                                                        <span>Загрузить</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="cart__upload">
                                                        <input type="file" name="file">
                                                        <strong>2 сторона</strong>
                                                        <span>Загрузить</span>
                                                    </label>
                                                </li>
                                            </ul>
                                            <div class="cart__upload_text">
                                                * Скан или фотография<br/>
                                                Формат файла jpg/JPG/jpeg
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Цена</div>
                                        <div class="cart__source">
                                            <div class="cart__price"><strong>2000</strong> <i class="ruble">¤</i></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__source">
                                            <button class="btn btn_del" type="button">
                                                <span>Удалить из<br/>корзины</span>
                                            </button>
                                        </div>

                                    </li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_3">
                                                        <div class="number__image">
                                                            <img src="img/number3.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="mb_20"></div>
                                                        <div class="mb_20 text-center">
                                                            <button class="btn btn_reg" type="button">
                                                                <i><img src="img/icon__save.svg" class="img-fluid" alt=""></i>
                                                                <span>Сохранить номер</span>
                                                            </button>
                                                        </div>

                                                        <div class="chars chars__type_3_1">
                                                            <input type="text" class="num_23a" name="num_23a" value="" placeholder="мм090">
                                                        </div>
                                                        <div class="chars chars__type_3_2">
                                                            <input type="text" class="num_23b" name="num_23b" value="" placeholder="78">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_4">
                                                        <div class="number__image">
                                                            <img src="img/number4.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_4_1">
                                                            <input type="text" class="num_20a" name="num_20a" value="" placeholder="мм090">
                                                        </div>
                                                        <div class="chars chars__type_4_2">
                                                            <input type="text" class="num_20b" name="num_20b" value="" placeholder="78">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_5">
                                                        <div class="number__image">
                                                            <img src="img/number5.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_5_1">
                                                            <input type="text" class="num_25a" name="num_25a" value="" placeholder="133">
                                                        </div>
                                                        <div class="chars chars__type_5_2">
                                                            <input type="text" class="num_25b" name="num_25b" value="" placeholder="АА">
                                                        </div>
                                                        <div class="chars chars__type_5_3">
                                                            <input type="text" class="num_25c" name="num_25c" value="" placeholder="47">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_6">
                                                        <div class="number__image">
                                                            <img src="img/number6.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_6_1">
                                                            <input type="text" class="num_22a" name="num_22a" value="" placeholder="133">
                                                        </div>
                                                        <div class="chars chars__type_6_2">
                                                            <input type="text" class="num_22b" name="num_22b" value="" placeholder="АА">
                                                        </div>
                                                        <div class="chars chars__type_6_3">
                                                            <input type="text" class="num_22c" name="num_22c" value="" placeholder="47">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_7">
                                                        <div class="number__image">
                                                            <img src="img/number7.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_7_1">
                                                            <input type="text" class="num_24a" name="num_24a" value="" placeholder="ММ">
                                                        </div>
                                                        <div class="chars chars__type_7_2">
                                                            <input type="text" class="num_24b" name="num_24b" value="" placeholder="976">
                                                        </div>
                                                        <div class="chars chars__type_7_3">
                                                            <input type="text" class="num_25c" name="num_25c" value="" placeholder="78">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_8">
                                                        <div class="number__image">
                                                            <img src="img/number8.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_8_1">
                                                            <input type="text" class="num_21a" name="num_21a" value="" placeholder="ММ">
                                                        </div>
                                                        <div class="chars chars__type_8_2">
                                                            <input type="text" class="num_21b" name="num_21b" value="" placeholder="976">
                                                        </div>
                                                        <div class="chars chars__type_8_3">
                                                            <input type="text" class="num_21c" name="num_21c" value="" placeholder="78">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_9">
                                                        <div class="number__image">
                                                            <img src="img/number9.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_9_1">
                                                            <input type="text" class="num_19a" name="num_19a" value="" placeholder="1133">
                                                        </div>
                                                        <div class="chars chars__type_9_2">
                                                            <input type="text" class="num_19b" name="num_19b" value="" placeholder="А">
                                                        </div>
                                                        <div class="chars chars__type_9_3">
                                                            <input type="text" class="num_19c" name="num_19c" value="" placeholder="77">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_10">
                                                        <div class="number__image">
                                                            <img src="img/number10.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_10_1">
                                                            <input type="text" class="num_13a" name="num_13a" value="" placeholder="1133">
                                                        </div>
                                                        <div class="chars chars__type_10_2">
                                                            <input type="text" class="num_13b" name="num_13b" value="" placeholder="АА">
                                                        </div>
                                                        <div class="chars chars__type_10_3">
                                                            <input type="text" class="num_13c" name="num_13c" value="" placeholder="77">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_11">
                                                        <div class="number__image">
                                                            <img src="img/number11.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_11_1">
                                                            <input type="text" class="num_18a" name="num_18a" value="" placeholder="976">
                                                        </div>
                                                        <div class="chars chars__type_11_2">
                                                            <input type="text" class="num_18b" name="num_18b" value="" placeholder="м">
                                                        </div>
                                                        <div class="chars chars__type_11_3">
                                                            <input type="text" class="num_18c" name="num_18c" value="" placeholder="77">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_12">
                                                        <div class="number__image">
                                                            <img src="img/number12.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_12_1">
                                                            <input type="text" class="num_17a" name="num_17a" value="" placeholder="м">
                                                        </div>
                                                        <div class="chars chars__type_12_2">
                                                            <input type="text" class="num_17b" name="num_17b" value="" placeholder="9768">
                                                        </div>
                                                        <div class="chars chars__type_12_3">
                                                            <input type="text" class="num_17c" name="num_17c" value="" placeholder="77">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_13">
                                                        <div class="number__image">
                                                            <img src="img/number13.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_13_1">
                                                            <input type="text" class="num_16a" name="num_16a" value="" placeholder="D">
                                                        </div>
                                                        <div class="chars chars__type_13_2">
                                                            <input type="text" class="num_16b" name="num_16b" value="" placeholder="876">
                                                        </div>
                                                        <div class="chars chars__type_13_3">
                                                            <input type="text"  class="num_16c" name="num_16c" value="" placeholder="23">
                                                        </div>
                                                        <div class="chars chars__type_13_4">
                                                            <input type="text" class="num_16d" name="num_16d" value="" placeholder="77">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_14">
                                                        <div class="number__image">
                                                            <img src="img/number14.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_14_1">
                                                            <input type="text" class="num_15a" name="num_15a" value="" placeholder="876">
                                                        </div>
                                                        <div class="chars chars__type_14_2">
                                                            <input type="text" class="num_15b" name="num_15b" value="" placeholder="234">
                                                        </div>
                                                        <div class="chars chars__type_14_3">
                                                            <input type="text" class="num_15c" name="num_15c" value="" placeholder="99">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_15">
                                                        <div class="number__image">
                                                            <img src="img/number15.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_15_1">
                                                            <input type="text" class="num_14a" name="num_14a" value="" placeholder="876">
                                                        </div>
                                                        <div class="chars chars__type_15_2">
                                                            <input type="text" class="num_14d" name="num_14d" value="" placeholder="С">
                                                        </div>
                                                        <div class="chars chars__type_15_3">
                                                            <input type="text" class="num_14b" name="num_14b" value="" placeholder="2">
                                                        </div>
                                                        <div class="chars chars__type_15_4">
                                                            <input type="text" class="num_14c" name="num_14c" value="" placeholder="99">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_16">
                                                        <div class="number__image">
                                                            <img src="img/number16.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_16_1">
                                                            <input type="text" class="num_3a" name="num_3a" value="" placeholder="ом">
                                                        </div>
                                                        <div class="chars chars__type_16_2">
                                                            <input type="text" class="num_3b" name="num_3b" value="" placeholder="958">
                                                        </div>
                                                        <div class="chars chars__type_16_3">
                                                            <input type="text" class="num_3c" name="num_3c" value="" placeholder="99">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_17">
                                                        <div class="number__image">
                                                            <img src="img/number17.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_17_1">
                                                            <input type="text" class="num_4a" name="num_4a" value="" placeholder="мм">
                                                        </div>
                                                        <div class="chars chars__type_17_2">
                                                            <input type="text" class="num_4b" name="num_4b" value="" placeholder="9768">
                                                        </div>
                                                        <div class="chars chars__type_17_3">
                                                            <input type="text" class="num_4c" name="num_4c" value="" placeholder="77">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_18">
                                                        <div class="number__image">
                                                            <img src="img/number18.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_18_1">
                                                            <input type="text" class="num_2a" name="num_2a" value="" placeholder="В">
                                                        </div>
                                                        <div class="chars chars__type_18_2">
                                                            <input type="text" class="num_2b" name="num_2b" value="" placeholder="555">
                                                        </div>
                                                        <div class="chars chars__type_18_3">
                                                            <input type="text" class="num_2c" name="num_2c" value="" placeholder="рх">
                                                        </div>
                                                        <div class="chars chars__type_18_4">
                                                            <input type="text" class="num_2d" name="num_2d" value="" placeholder="39">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_19">
                                                        <div class="number__image">
                                                            <img src="img/number19.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_19_1">
                                                            <input type="text" class="num_8" name="num_8b" value="" placeholder="3773">
                                                        </div>
                                                        <div class="chars chars__type_19_2">
                                                            <input type="text" class="num_8b" name="num_8b" value="" placeholder="мм">
                                                        </div>
                                                        <div class="chars chars__type_19_3">
                                                            <input type="text" class="num_8c" name="num_8c" value="" placeholder="55">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_20">
                                                        <div class="number__image">
                                                            <img src="img/number20.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_20_1">
                                                            <input type="text" class="num_5a" name="num_5a" value="" placeholder="1133">
                                                        </div>
                                                        <div class="chars chars__type_20_2">
                                                            <input type="text" class="num_5b" name="num_5b" value="" placeholder="АА">
                                                        </div>
                                                        <div class="chars chars__type_20_3">
                                                            <input type="text" class="num_5c" name="num_5c" value="" placeholder="77">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_21">
                                                        <div class="number__image">
                                                            <img src="img/number21.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_21_1">
                                                            <input type="text" class="num_6b" name="num_6b" value="" placeholder="АА">
                                                        </div>
                                                        <div class="chars chars__type_21_2">
                                                            <input type="text" class="num_6c" name="num_6c" value="" placeholder="77">
                                                        </div>
                                                        <div class="chars chars__type_21_3">
                                                            <input type="text" class="num_6a" name="num_6a" value="" placeholder="1133">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_22">
                                                        <div class="number__image">
                                                            <img src="img/number22.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_22_1">
                                                            <input type="text" class="num_12a" name="num_12a" value="" placeholder="3773">
                                                        </div>
                                                        <div class="chars chars__type_22_2">
                                                            <input type="text" class="num_12b" name="num_12b" value="" placeholder="мм">
                                                        </div>
                                                        <div class="chars chars__type_22_3">
                                                            <input type="text" class="num_12c" name="num_12c" value="" placeholder="55">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_23">
                                                        <div class="number__image">
                                                            <img src="img/number23.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_23_1">
                                                            <input type="text" class="num_11a" name="num_11a" value="" placeholder="мм7976">
                                                        </div>
                                                        <div class="chars chars__type_23_2">
                                                            <input type="text" class="num_11b" name="num_11b" value="" placeholder="99">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_24">
                                                        <div class="number__image">
                                                            <img src="img/number24.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_24_1">
                                                            <input type="text" class="num_10a" name="num_10a" value="" placeholder="7976мм">
                                                        </div>
                                                        <div class="chars chars__type_24_2">
                                                            <input type="text" class="num_10b" name="num_10b" value="" placeholder="99">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_25">
                                                        <div class="number__image">
                                                            <img src="img/number25.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_25_1">
                                                            <input type="text" class="num_9a" name="num_9a" value="" placeholder="мм976">
                                                        </div>
                                                        <div class="chars chars__type_25_2">
                                                            <input type="text" class="num_9b" name="num_9b" value="" placeholder="78">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <br/>
                                        <div class="cart__source">
                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_26">
                                                        <div class="number__image">
                                                            <img src="img/number26.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_26_1">
                                                            <input type="text" class="num_7b" name="num_7b" value="" placeholder="мм">
                                                        </div>
                                                        <div class="chars chars__type_26_2">
                                                            <input type="text" class="num_7a" name="num_7a" value="" placeholder="55">
                                                        </div>
                                                        <div class="chars chars__type_26_3">
                                                            <input type="text" class="num_7c" name="num_7c" value="" placeholder="АА">
                                                        </div>
                                                        <div class="chars chars__type_26_4">
                                                            <input type="text" class="num_7d" name="num_7d" value="" placeholder="77">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>


                                <ul class="cart__row">
                                    <li>
                                        <div class="cart__heading">Товар</div>
                                        <div class="cart__source">

                                            <div class="cart__auto">
                                                <div class="cart__auto_icon">тип <strong>1</strong></div>
                                                <div class="cart__auto_text">Легковые, грузовые и автобусы</div>
                                            </div>

                                            <ul class="cart__params">
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1">
                                                        <span>с отверстиями</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1">
                                                        <span>с флагом</span>
                                                    </label>
                                                </li>
                                            </ul>

                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_1">
                                                        <div class="number__image">
                                                            <img src="img/number.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_1_1">
                                                            <input type="text" class="num_1a" name="num_1a" value="" placeholder="м090мм">
                                                        </div>
                                                        <div class="chars chars__type_1_2">
                                                            <input type="text" class="num_1b" name="num_1b" value="" placeholder="090">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="cart__btn">

                                                <button class="btn btn_reg" type="button">
                                                    <i><img src="img/icon__edit.svg" class="img-fluid" alt=""></i>
                                                    <span>Редактировать номер</span>
                                                </button>

                                                <button class="btn btn_white btn_del" type="button">
                                                    <span>Удалить из<br/>корзины</span>
                                                </button>

                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Количество</div>
                                        <div class="cart__source">

                                            <ul class="cart__amount">
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1" checked>
                                                        <span>2 шт.</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1">
                                                        <span>1 шт.</span>
                                                    </label>
                                                </li>
                                            </ul>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Документы</div>
                                        <div class="cart__source">
                                            <h4>Свидететельство о регистрации ТС или ПТС*</h4>
                                            <ul class="cart__docs">
                                                <li>
                                                    <label class="cart__upload">
                                                        <input type="file" name="file">
                                                        <strong>1 сторона</strong>
                                                        <span>Загрузить</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="cart__upload">
                                                        <input type="file" name="file">
                                                        <strong>2 сторона</strong>
                                                        <span>Загрузить</span>
                                                    </label>
                                                </li>
                                            </ul>
                                            <div class="cart__upload_text">
                                                * Скан или фотография<br/>
                                                Формат файла jpg/JPG/jpeg
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Цена</div>
                                        <div class="cart__source">
                                            <div class="cart__price"><strong>2000</strong> <i class="ruble">¤</i></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__source">
                                            <button class="btn btn_del" type="button">
                                                <span>Удалить из<br/>корзины</span>
                                            </button>
                                        </div>

                                    </li>
                                </ul>


                            </div>

                        </div>

                        <div class="checkout__consent">
                            <div class="checkout__consent_wrap">
                                <label class="form_checkbox error">
                                    <input type="checkbox" name="consent">
                                    <span>Согласен(а) на обработку персональных данных в соответствии с <a href="#">Пользовательским соглашением</a> ООО «ПТФ Метапласт»</span>
                                </label>
                            </div>
                        </div>

                        <div class="checkout__block">

                            <ul class="purchaser_type">
                                <li>
                                    <label class="form_radio form_radio_md">
                                        <input type="radio" name="purchaser_type" value="private" checked>
                                        <span>Физическое лицо</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form_radio form_radio_md">
                                        <input type="radio" name="purchaser_type" value="corp">
                                        <span>Юридическое лицо</span>
                                    </label>
                                </li>
                            </ul>

                            <div class="checkout__group">

                                <div class="purchaser_tab active" id="private">

                                    <div class="purchaser_private">
                                        <div class="purchaser_private__form">
                                            <div class="form_group">
                                                <input class="form_control error" type="text" name="name" placeholder="ФИО *">
                                            </div>
                                            <div class="form_group">
                                                <input class="form_control error" type="text" name="phone" placeholder="Телефон *">
                                            </div>
                                            <div class="form_group">
                                                <input class="form_control" type="text" name="email" placeholder="Email *">
                                            </div>
                                            <div class="form_group">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="check">
                                                    <span>Я прочитал(а) и согласен(а) с условиями <a href="#">Договора оферты</a></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="purchaser_private__docs">

                                            <h4>Загрузить документ удостоверяющий личность:</h4>
                                            <p class="text-center mb_25 hide-sm">*Формат файла JPG, PNG</p>

                                            <ul>
                                                <li>
                                                    <p>
                                                        Паспорт 1-я страница<br/>
                                                        или Водительское удостоверение сторона 1*
                                                    </p>
                                                    <div class="docs">
                                                        <div class="docs__row">
                                                            <div class="docs__passport">
                                                                <img src="img/doc__xl.svg" class="img-fluid" alt="">
                                                            </div>
                                                            <div class="docs__text">или</div>
                                                            <div class="docs__license">
                                                                <img src="img/doc__sm.svg" class="img-fluid" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <label class="docs__upload">
                                                                <input type="file" value="">
                                                                <span class="docs__upload_text">Загрузить</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <small class="hide-xs-only">*Формат файла JPG, PNG</small>
                                                </li>
                                                <li>
                                                    <p>
                                                        Паспорт страница регистрации<br/>
                                                        или Водительское удостоверение сторона 2*
                                                    </p>
                                                    <div class="docs">
                                                        <div class="docs__row">
                                                            <div class="docs__passport">
                                                                <img src="img/doc__xl.svg" class="img-fluid" alt="">
                                                            </div>
                                                            <div class="docs__text">или</div>
                                                            <div class="docs__license">
                                                                <img src="img/doc__sm.svg" class="img-fluid" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <label class="docs__upload">
                                                                <input type="file" value="">
                                                                <span class="docs__upload_text">Загрузить</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>

                                </div>

                                <div class="purchaser_tab" id="corp">
                                    <div class="purchaser_corp">
                                        <div class="purchaser_corp__left">
                                            <div class="form_group">
                                                <input class="form_control" type="text" name="name" placeholder="Контактное лицо *">
                                            </div>
                                            <div class="form_group">
                                                <input class="form_control" type="text" name="email" placeholder="e-mail *">
                                            </div>
                                            <div class="form_group">
                                                <input class="form_control" type="text" name="phone" placeholder="Телефон *">
                                            </div>
                                        </div>
                                        <div class="purchaser_corp__right">
                                            <div class="form_group">
                                                <input class="form_control" type="text" name="company" placeholder="Наименование юридического лица в соответствии с учр. документами *">
                                            </div>
                                            <div class="form_group">
                                                <label class="docs_file">
                                                    <input type="file" name="docs">
                                                    <div class="docs_file__text">
                                                        <strong>Загрузить карточку Организации</strong>
                                                        <span>Для формирования счета-фактуры, товарных накладных</span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="form_group">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="name" value="">
                                                    <span>Я прочитал(а) и согласен(а) с условиями Договора оферты</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="checkout__group">

                                <ul class="delivery_type">
                                    <li>
                                        <label class="form_radio form_radio_md">
                                            <input type="radio" name="delivery_type" value="pickup">
                                            <span>Самовывоз</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="form_radio form_radio_md">
                                            <input type="radio" name="delivery_type" value="delivery" checked>
                                            <span>Доставка</span>
                                        </label>
                                    </li>
                                </ul>

                                <div class="delivery active" id="delivery">

                                    <h4>Укажите адрес доставки для расчета стоимости:</h4>

                                    <ul class="delivery__form">
                                        <li>
                                            <input class="form_control" type="text" name="city" placeholder="Город…">
                                        </li>
                                        <li>
                                            <input class="form_control" type="text" name="zip" placeholder="Индекс">
                                        </li>
                                        <li>
                                            <input class="form_control" type="text" name="street" placeholder="Улица">
                                        </li>
                                    </ul>

                                    <table class="delivery__table">
                                        <tr>
                                            <th>Способы доставки:</th>
                                            <th>Описание доставки</th>
                                            <th>Цена доставки</th>
                                            <th class="hide-xs-only">Стоимость заказа</th>
                                            <th class="hide-xs-only">Итого</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="form_radio">
                                                    <input type="radio" name="delivery_place" value="1" checked>
                                                    <span>Постомат</span>
                                                </label>
                                            </td>
                                            <td>
                                                <a class="delivery__place" href="#">
                                                    <span>Выберите постомат на карте</span>
                                                    <i><img src="img/icon__place.svg" class="img-fluid" alt="">
                                                    </i>
                                                </a>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><strong>300</strong> ₽</div>
                                                <div class="post_days">5 дней</div>
                                            </td>
                                            <td class="hide-xs-only"></td>
                                            <td class="hide-xs-only"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="form_radio">
                                                    <input type="radio" name="delivery_place" value="1">
                                                    <span>Почта России</span>
                                                </label>
                                            </td>
                                            <td>Доставка по всей России</td>
                                            <td>
                                                <div class="delivery__price"><strong>300</strong> ₽</div>
                                                <div class="post_days">5 дней</div>
                                            </td>
                                            <td class="hide-xs-only"></td>
                                            <td class="hide-xs-only"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="form_radio">
                                                    <input type="radio" name="delivery_place" value="1">
                                                    <span>Курьер</span>
                                                </label>
                                            </td>
                                            <td>Экспресс-доставка по всей России</td>
                                            <td>
                                                <div class="delivery__price"><strong>300</strong> ₽</div>
                                                <div class="post_days">5 дней</div>
                                            </td>
                                            <td class="hide-xs-only"></td>
                                            <td class="hide-xs-only"></td>
                                        </tr>
                                    </table>

                                    <div class="delivery__total">
                                        <div class="delivery__total_price"><span>Итого:</span> 300 ₽</div>
                                        <div class="delivery__total_nds">Цены включают НДС</div>
                                    </div>
                                </div>

                                <div class="delivery" id="pickup">

                                    <div class="delivery__pickup">
                                        <div class="delivery__pickup_title">Адрес, где Вы сможете самостоятельно забрать товар:</div>
                                        <div class="delivery__pickup_text">Москва, Локомотивный проезд …..</div>
                                    </div>

                                    <table class="delivery__table">
                                        <tr>
                                            <th>Способ оплаты:</th>
                                            <th>Стоимость самовывоза</th>
                                            <th>Стоимость заказа</th>
                                            <th>Итого</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="form_radio">
                                                    <input type="radio" name="delivery_place" value="1" checked>
                                                    <span>Наличными или банковской картой при получении</span>
                                                </label>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><strong>300</strong> ₽</div>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><span>2100</span> ₽</div>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><strong>2400</strong> ₽</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="form_radio">
                                                    <input type="radio" name="delivery_place" value="1" checked>
                                                    <span>Банковской картой на сайте</span>
                                                </label>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><strong>300</strong> ₽</div>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><span>2100</span> ₽</div>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><strong>2400</strong> ₽</div>
                                            </td>
                                        </tr>
                                    </table>

                                    <div class="delivery__summary">Цены включают НДС</div>

                                </div>


                            </div>

                        </div>

                        <div class="checkout__accept">
                            <button class="btn btn_yellow btn_md" type="submit"><span>ОФОРМИТЬ ЗАКАЗ И ОПЛАТИТЬ</span></button>
                        </div>

                    </div>


                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
