<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page" id="top">

            <!-- Header -->
            <div class="header_mobile">
                <div class="container">
                    <div class="header_mobile__wrap">
                        <a class="header_mobile__phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                        <a class="header_mobile__email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header_mobile__slogan">Производим автомобильные номера с 1991 года</div>
                    <div class="header__row">
                        <div class="header__left">
                            <a class="header__toggle nav_toggle" href="#"></a>
                            <a class="header__logo" href="/">
                                <img src="img/logo.svg" class="img-fluid" alt="">
                            </a>
                            <div class="header__slogan">Производим автомобильные номера с 1991 года</div>
                        </div>
                        <nav class="header__nav">
                            <ul>
                                <li><a href="#">О Компании</a></li>
                                <li><a href="#">Аксессуары</a></li>
                                <li><a href="#">Доставка и оплата</a></li>
                                <li><a href="#">Для производителей номеров</a></li>
                                <li class="active"><a href="#">Контакты</a></li>
                            </ul>
                        </nav>
                        <div class="header__right">

                            <div class="header__slogan_tablet">Производим автомобильные номера с 1991 года</div>

                            <ul class="header__contact">
                                <li>
                                    <a class="header__contact_phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                                </li>
                                <li>
                                    <a class="header__contact_email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                                </li>
                            </ul>

                            <div class="header__cart">
                                <span>2</span>
                                <i>
                                    <img src="img/icon__cart.svg" class="img-fluid" alt="">
                                </i>
                                <a href="#" class="header__cart_link cart_toggle"></a>
                                <div class="mini_cart">
                                    <div class="mini_cart__heading">
                                        <strong>Корзина</strong>
                                        <span>В Вашей корзине 2 товара</span>
                                    </div>
                                    <div class="mini_cart__close cart_toggle"></div>
                                    <div class="mini_cart__content">
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>1</strong>
                                                    </div>
                                                    <a href="#">Легковые грузовые и автобусы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image">
                                                    <img src="images/mini_cat__item_01.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>1 200</strong> ₽</li>
                                        </ul>
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>3</strong>
                                                    </div>
                                                    <a href="#">Дорожно-строительная техника, тракторы, прицепы и полуприцепы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image mini_cart__image_small">
                                                    <img src="images/mini_cat__item_02.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>900</strong> ₽</li>
                                        </ul>
                                    </div>
                                    <div class="mini_cart__footer">
                                        <a href="#" class="btn btn_sm btn_yellow">Перейти в корзину</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>



                </div>
            </header>

            <nav class="nav_mobile">
                <span class="nav_mobile__close nav_toggle"></span>
                <div class="nav_mobile__logo">
                    <img src="img/logo.svg" class="img-fluid" alt="">
                </div>
                <ul>
                    <li><a href="#">Интренет магазин</a></li>
                    <li><a href="#">О Компании</a></li>
                    <li><a href="#">Аксессуары</a></li>
                    <li><a href="#">Доставка и оплата</a></li>
                    <li><a href="#">Для производителей номеров</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </nav>
            <div class="nav_mobile__layout nav_toggle"></div>
            <!-- -->

            <section class="main">
                <div class="container">

                    <h1 class="text-center">Контакты</h1>

                    <div class="contact">

                        <div class="contact__content">
                            <div class="contact__about">
                                ООО «Производственно-Техническая Фирма «МЕТАПЛАСТ»<br/>
                                ИНН 7713388491 КПП 771301001<br/>
                                Юридический адрес: 127238, Москва, Локомотивный проезд, д.21 стр.3
                            </div>
                            <ul class="contact__data">
                                <li>
                                    <i>
                                        <img src="img/icon__contact_phone.svg" class="ico-svg" alt="">
                                    </i>
                                    <span>Тел.: <a href="tel:+74952251399">+7 (495) 225-13-99</a></span>
                                </li>
                                <li>
                                    <i>
                                        <img src="img/icon__contact_email.svg" class="ico-svg" alt="">
                                    </i>
                                    <span>e-mail: <a href="mailto:info@mtplast.ru">info@mtplast.ru</a></span>
                                </li>
                                <li>
                                    <i>
                                        <img src="img/icon__contact_whatsapp.svg" class="ico-svg" alt="">
                                    </i>
                                    <span>WhatsApp: <a href="tel:+74952251399">+7 (495) 225-13-99</a></span>
                                </li>
                                <li>
                                    <i>
                                        <img src="img/icon__contact_card.svg" class="ico-svg" alt="">
                                    </i>
                                    <span><a href="#">Скачать карточку Компании ></a></span>
                                </li>
                            </ul>
                            <div class="contact__subtitle">Схема проезда в офис и пункт самовывоза</div>
                            <div class="contact__map" id="map">

                            </div>
                        </div>

                        <div class="contact__form">
                            <div class="contact__form_text">Вы можете отправить нам сообщение с помощью контактной формы</div>
                            <form class="form">
                                <div class="row">
                                    <div class="col col-xs-12 col-gutter-lr">
                                        <div class="form_group">
                                            <input type="text" class="form_control" name="name" placeholder="Ваше Имя*">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                        <div class="form_group">
                                            <input type="text" class="form_control" name="phone" placeholder="Телефон*">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                        <div class="form_group">
                                            <input type="text" class="form_control" name="email" placeholder="e-mail*">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-gutter-lr">
                                        <div class="form_group">
                                            <textarea class="form_control" name="message" placeholder="Текст сообщения*" rows="8"></textarea>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_checkbox form_checkbox_sm">
                                                <input type="checkbox" name="check" checked>
                                                <span>Согласен(а) на обработку персональных данных в соответствии с Пользовательским соглашением ООО «ПТФ Метапласт»</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-gutter-lr contact__form_submit">
                                        <button type="submit" class="btn btn_yellow btn_md">Отправить сообщение</button>
                                    </div>

                                </div>
                            </form>
                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

        <script>
            ymaps.ready(init);

            function init () {
                var myMap = new ymaps.Map("map", {
                    center: [55.844960568877916,37.57180299999996],
                    zoom: 15,
                    controls: ['smallMapDefaultSet']
                });

                myMap.geoObjects
                    .add(new ymaps.Placemark([55.844960568877916,37.57180299999996], {
                        balloonContent: ''
                    }, {
                        preset: 'islands#icon',
                        iconColor: '#0095b6'
                    }));
            }
        </script>

    </body>
</html>
