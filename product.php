<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page" id="top">

            <!-- Header -->
            <div class="header_mobile">
                <div class="container">
                    <div class="header_mobile__wrap">
                        <a class="header_mobile__phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                        <a class="header_mobile__email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header_mobile__slogan">Производим автомобильные номера с 1991 года</div>
                    <div class="header__row">
                        <div class="header__left">
                            <a class="header__toggle nav_toggle" href="#"></a>
                            <a class="header__logo" href="/">
                                <img src="img/logo.svg" class="img-fluid" alt="">
                            </a>
                            <div class="header__slogan">Производим автомобильные номера с 1991 года</div>
                        </div>
                        <nav class="header__nav">
                            <ul>
                                <li><a href="#">О Компании</a></li>
                                <li><a href="#">Аксессуары</a></li>
                                <li><a href="#">Доставка и оплата</a></li>
                                <li><a href="#">Для производителей номеров</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </nav>
                        <div class="header__right">

                            <div class="header__slogan_tablet">Производим автомобильные номера с 1991 года</div>

                            <ul class="header__contact">
                                <li>
                                    <a class="header__contact_phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                                </li>
                                <li>
                                    <a class="header__contact_email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                                </li>
                            </ul>

                            <div class="header__cart">
                                <span>2</span>
                                <i>
                                    <img src="img/icon__cart.svg" class="img-fluid" alt="">
                                </i>
                                <a href="#" class="header__cart_link cart_toggle"></a>
                                <div class="mini_cart">
                                    <div class="mini_cart__heading">
                                        <strong>Корзина</strong>
                                        <span>В Вашей корзине 2 товара</span>
                                    </div>
                                    <div class="mini_cart__close cart_toggle"></div>
                                    <div class="mini_cart__content">
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>1</strong>
                                                    </div>
                                                    <a href="#">Легковые грузовые и автобусы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image">
                                                    <img src="images/mini_cat__item_01.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>1 200</strong> ₽</li>
                                        </ul>
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>3</strong>
                                                    </div>
                                                    <a href="#">Дорожно-строительная техника, тракторы, прицепы и полуприцепы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image mini_cart__image_small">
                                                    <img src="images/mini_cat__item_02.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>900</strong> ₽</li>
                                        </ul>
                                    </div>
                                    <div class="mini_cart__footer">
                                        <a href="#" class="btn btn_sm btn_yellow">Перейти в корзину</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <ul class="header__breadcrumbs">
                            <li><a href="#">Интернет-магазин</a></li>
                            <li><a href="#">Гражданские</a></li>
                            <li><span>Легковые, грузовые и автобусы</span></li>
                        </ul>

                    </div>

                </div>
            </header>

            <nav class="nav_mobile">
                <span class="nav_mobile__close nav_toggle"></span>
                <div class="nav_mobile__logo">
                    <img src="img/logo.svg" class="img-fluid" alt="">
                </div>
                <ul>
                    <li><a href="#">Интренет магазин</a></li>
                    <li><a href="#">О Компании</a></li>
                    <li><a href="#">Аксессуары</a></li>
                    <li><a href="#">Доставка и оплата</a></li>
                    <li><a href="#">Для производителей номеров</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </nav>
            <div class="nav_mobile__layout nav_toggle"></div>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="main_row">

                        <div class="sidebar">
                            <div class="sidebar__nav">
                                <a href="#" class="catalog__type catalog__type_civil">Гражданские</a>
                                <a href="#" class="catalog__type catalog__type_mil">Военные</a>
                                <a href="#"class="catalog__type catalog__type_diplomatic">Дипломатические</a>
                                <a href="#" class="catalog__type catalog__type_police">МВД</a>
                                <a href="#" class="catalog__type catalog__type_other">Другие</a>
                            </div>
                        </div>

                        <div class="product">

                            <div class="product__block">

                                <div class="product__heading">
                                    <div class="product__heading_type"><span>тип</span> <strong>1</strong></div>
                                    <div class="product__heading_name">Легковые, грузовые и автобусы</div>
                                </div>

                                <div class="product__row">

                                    <div class="product__main">

                                        <div class="product__num">
                                            <div class="num num__01">
                                                <img src="img/num/num_01.svg" class="img-fluid" alt="">
                                                <div class="num__char num__01_a">
                                                    <input type="text" class="num_1a" name="num_1a" value="" placeholder="м976мм">
                                                </div>
                                                <div class="num__char num__01_b">
                                                    <input type="text" class="num_1b" name="num_1b" value="" placeholder="090">
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="product__params">
                                            <li>
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="params">
                                                    <span>с отверстиями</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="params">
                                                    <span>с флагом</span>
                                                </label>
                                            </li>
                                        </ul>
                                        <div class="product__edit">
                                            <button class="btn btn_reg" type="button">
                                                <i><img src="img/icon__edit.svg" class="img-fluid" alt=""></i>
                                                <span>Редактировать номер</span>
                                            </button>
                                        </div>

                                    </div>

                                    <div class="product__order">
                                        <div class="product__order_wrap">
                                            <ul class="product__order_label">
                                                <li>Количество:</li>
                                                <li>Цена:</li>
                                            </ul>
                                            <ul class="product__order_item">
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="order_params">
                                                        <span>1 номер</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <div class="product__order_price"><strong>600</strong> ₽</div>
                                                </li>
                                            </ul>
                                            <ul class="product__order_item">
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="order_params">
                                                        <span>2 номера</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <div class="product__order_price"><strong>1 200</strong> <i class="ruble">¤</i></div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="product__order_button">
                                            <button type="submit" class="btn btn_md btn_yellow">Добавить в корзину</button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="product__block">
                                <div class="product__block_heading">Преимущества</div>
                                <ul class="product__advantage">
                                    <li>
                                        <i>
                                            <img src="img/product_advantage__01.svg">
                                        </i>
                                        <span>Лицензия<br/> № 344566778</span>
                                    </li>
                                    <li>
                                        <i>
                                            <img src="img/product_advantage__02.svg">
                                        </i>
                                        <span>Лучшая цена</span>
                                    </li>
                                    <li>
                                        <i>
                                            <img src="img/product_advantage__03.svg">
                                        </i>
                                        <span>Изготовление<br/>за 1 день</span>
                                    </li>
                                    <li>
                                        <i>
                                            <img src="img/product_advantage__04.svg">
                                        </i>
                                        <span>Быстрая<br/>доставка</span>
                                    </li>
                                    <li>
                                        <i>
                                            <img src="img/product_advantage__05.svg">
                                        </i>
                                        <span>Соответствует<br/>требованиям<br/>ГИБДД и ГОСТ</span>
                                    </li>
                                    <li>
                                        <i>
                                            <img src="img/product_advantage__06.svg">
                                        </i>
                                        <span>Собственное<br/>производство</span>
                                    </li>
                                    <li>
                                        <i>
                                            <img src="img/product_advantage__07.svg">
                                        </i>
                                        <span>Высококачественные<br/>материалы</span>
                                    </li>
                                </ul>
                            </div>

                            <div class="product__block">
                                <div class="product__block_heading">Описание</div>

                                <p>На первом экране карточки товара располагается интерактивный номерной знак с возможностью ввода необходимых данных для добавления в корзину. Данные (цифры и буквы) должны визуально моделироваться в реальном времени на интерактивном знаке.</p>

                                <p>На первом экране карточки товара должен находиться тип номера.</p>

                                <p>На первом экране карточки товара должна находиться кнопка «добавить в корзину» (для уменьшения времени покупки).</p>

                                <p>В карточке товара должны находиться преимущества заказа в Метапласте. Они должны быть оформлены в виде иконок и подписей к ним. Преимущества (необходимо сохранить последовательность): Лицензия № (иконка лицензии) Лучшая цена (иконка на выбор дизайнеров) Изготовление за 1 день (иконка часы) Быстрая доставка (иконка автомобиля) Соответствует требованиям ГИБДД и ГОСТ (иконка на выбор дизайнеров) Собственное производство (любая иконка, ассоциирующаяся с производством)<br/>
                                Высококачественные материалы (иконка на выбор дизайнеров)</p>

                                <p>В карточке товара должно находиться описание товара (порядка 1000 знаков без пробелов).</p>

                                <p>В карточке товара (ниже описания) должны находиться скан-копии сертификатов и лицензий (порядка 3 штук).</p>

                                <p>В карточке товара должны находиться примеры продукции (галерея изображений).</p>


                            </div>

                            <div class="product__block">
                                <div class="product__block_heading">Сертификаты</div>

                                <div class="product__block_row">
                                    <div class="product__block_col">
                                        <p>Для изготовления дубликатов государственных регистрационных знаков транспортных средств необходимо иметь  право на их изготовление и технические возможности (помещение, оборудование, расходные материалы, обученный персонал).</p>
                                        <p>Изготовление дубликатов государственных регистрационных знаков транспортных средств на законном основании возможно только при наличии у производителя Свидетельства об утверждении изготовленного образца специальной продукции – регистрационного знака, выдаваемого ГУ по ОБДД МВД России на основании приказа МВД РФ № 390 от 27.04.2002 г.</p>
                                    </div>
                                    <div class="product__block_col">
                                        <div class="product__certificate">
                                            <a href="images/certificate_01.jpg" class="product__certificate_item btn_modal">
                                                <img src="images/certificate_01.jpg" class="img-fluid" alt="">
                                            </a>
                                            <a href="images/certificate_01.jpg" class="product__certificate_item btn_modal">
                                                <img src="images/certificate_01.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="product__block">
                                <div class="product__block_heading">Наши работы</div>
                                <div class="product__gallery">
                                    <div class="product__gallery_wrap">
                                        <div class="swiper-container product__gallery_slider">
                                            <div class="swiper-wrapper">
                                                <!-- Slides -->
                                                <div class="swiper-slide">
                                                    <a href="images/gallery__01.jpg" class="product__gallery_item" data-fancybox>
                                                        <img src="images/gallery__01.jpg" class="img-fluid" alt="">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a href="images/gallery__02.jpg" class="product__gallery_item" data-fancybox>
                                                        <img src="images/gallery__02.jpg" class="img-fluid" alt="">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a href="images/gallery__03.jpg" class="product__gallery_item" data-fancybox>
                                                        <img src="images/gallery__03.jpg" class="img-fluid" alt="">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a href="images/gallery__04.jpg" class="product__gallery_item" data-fancybox>
                                                        <img src="images/gallery__04.jpg" class="img-fluid" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
