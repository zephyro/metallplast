<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page" id="top">

            <!-- Header -->
            <div class="header_mobile">
                <div class="container">
                    <div class="header_mobile__wrap">
                        <a class="header_mobile__phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                        <a class="header_mobile__email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header_mobile__slogan">Производим автомобильные номера с 1991 года</div>
                    <div class="header__row">
                        <div class="header__left">
                            <a class="header__toggle nav_toggle" href="#"></a>
                            <a class="header__logo" href="/">
                                <img src="img/logo.svg" class="img-fluid" alt="">
                            </a>
                            <div class="header__slogan">Производим автомобильные номера с 1991 года</div>
                        </div>
                        <nav class="header__nav">
                            <ul>
                                <li><a href="#">О Компании</a></li>
                                <li><a href="#">Аксессуары</a></li>
                                <li><a href="#">Доставка и оплата</a></li>
                                <li><a href="#">Для производителей номеров</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </nav>
                        <div class="header__right">

                            <div class="header__slogan_tablet">Производим автомобильные номера с 1991 года</div>

                            <ul class="header__contact">
                                <li>
                                    <a class="header__contact_phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                                </li>
                                <li>
                                    <a class="header__contact_email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                                </li>
                            </ul>

                            <div class="header__cart">
                                <span>2</span>
                                <i>
                                    <img src="img/icon__cart.svg" class="img-fluid" alt="">
                                </i>
                                <a href="#" class="header__cart_link cart_toggle"></a>
                                <div class="mini_cart">
                                    <div class="mini_cart__heading">
                                        <strong>Корзина</strong>
                                        <span>В Вашей корзине 2 товара</span>
                                    </div>
                                    <div class="mini_cart__close cart_toggle"></div>
                                    <div class="mini_cart__content">
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>1</strong>
                                                    </div>
                                                    <a href="#">Легковые грузовые и автобусы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image">
                                                    <img src="images/mini_cat__item_01.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>1 200</strong> ₽</li>
                                        </ul>
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>3</strong>
                                                    </div>
                                                    <a href="#">Дорожно-строительная техника, тракторы, прицепы и полуприцепы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image mini_cart__image_small">
                                                    <img src="images/mini_cat__item_02.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>900</strong> ₽</li>
                                        </ul>
                                    </div>
                                    <div class="mini_cart__footer">
                                        <a href="#" class="btn btn_sm btn_yellow">Перейти в корзину</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <ul class="header__breadcrumbs">
                            <li><a href="#">Интернет-магазин</a></li>
                            <li><a href="#">Гражданские</a></li>
                            <li><span>Легковые, грузовые и автобусы</span></li>
                        </ul>

                    </div>

                </div>
            </header>

            <nav class="nav_mobile">
                <span class="nav_mobile__close nav_toggle"></span>
                <div class="nav_mobile__logo">
                    <img src="img/logo.svg" class="img-fluid" alt="">
                </div>
                <ul>
                    <li><a href="#">Интренет магазин</a></li>
                    <li><a href="#">О Компании</a></li>
                    <li><a href="#">Аксессуары</a></li>
                    <li><a href="#">Доставка и оплата</a></li>
                    <li><a href="#">Для производителей номеров</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </nav>
            <div class="nav_mobile__layout nav_toggle"></div>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="main_row">

                        <div class="product">

                            <div class="product__block">

                                <div class="product__heading">

                                </div>

                                <div class="product__row">

                                    <div class="product__main">

                                        <!-- number 01 -->
                                        <div class="product__num">
                                            <div class="num num__01">
                                                <img src="img/num/num_01.svg" class="img-fluid" alt="">
                                                <div class="num__char num__01_a">
                                                    <input type="text" class="num_1a" name="num_1a" value="" placeholder="м976мм">
                                                </div>
                                                <div class="num__char num__01_b">
                                                    <input type="text" class="num_1b" name="num_1b" value="" placeholder="090">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 02 -->
                                        <div class="product__num">
                                            <div class="num num__02">
                                                <img src="img/num/num_02.svg" class="img-fluid" alt="">
                                                <div class="num__char num__02_a">
                                                    <input type="text" class="num_2a" name="num_2a" value="" placeholder="В">
                                                </div>
                                                <div class="num__char num__02_b">
                                                    <input type="text" class="num_2b" name="num_2b" value="" placeholder="555">
                                                </div>
                                                <div class="num__char num__02_c">
                                                    <input type="text" class="num_2c" name="num_2c" value="" placeholder="РХ">
                                                </div>
                                                <div class="num__char num__02_d">
                                                    <input type="text" class="num_8d" name="num_8d" value="" placeholder="125">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 03 -->
                                        <div class="product__num">

                                            <div class="num num__03">
                                                <img src="img/num/num_03.svg" class="img-fluid" alt="">
                                                <div class="num__char num__03_a">
                                                    <input type="text" class="num_3a" name="num_3a" value="" placeholder="ом">
                                                </div>
                                                <div class="num__char num__03_b">
                                                    <input type="text" class="num_3b" name="num_3b" value="" placeholder="958">
                                                </div>
                                                <div class="num__char num__03_c">
                                                    <input type="text" class="num_3c" name="num_3c" value="" placeholder="99">
                                                </div>
                                            </div>

                                        </div>
                                        <!-- -->

                                        <!-- number 04 -->
                                        <div class="product__num">
                                            <div class="num num__04">
                                                <img src="img/num/num_04.svg" class="img-fluid" alt="">
                                                <div class="num__char num__04_a">
                                                    <input type="text" class="num_4a" name="num_4a" value="" placeholder="мм">
                                                </div>
                                                <div class="num__char num__04_b">
                                                    <input type="text" class="num_4a" name="num_4b" value="" placeholder="9768">
                                                </div>
                                                <div class="num__char num__04_c">
                                                    <input type="text" class="num_4c" name="num_4c" value="" placeholder="77">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 05 -->
                                        <div class="product__num">
                                            <div class="num num__05">
                                                <img src="img/num/num_05.svg" class="img-fluid" alt="">
                                                <div class="num__char num__05_a">
                                                    <input type="text" class="num_5a" name="num_5a" value="" placeholder="1133">
                                                </div>
                                                <div class="num__char num__05_b">
                                                    <input type="text" class="num_5a" name="num_5b" value="" placeholder="АА">
                                                </div>
                                                <div class="num__char num__05_c">
                                                    <input type="text" class="num_5c" name="num_5c" value="" placeholder="77">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 06 -->
                                        <div class="product__num">
                                            <div class="num num__06">
                                                <img src="img/num/num_06.svg" class="img-fluid" alt="">
                                                <div class="num__char num__06_a">
                                                    <input type="text" class="num_6a" name="num_6a" value="" placeholder="АА">
                                                </div>
                                                <div class="num__char num__06_b">
                                                    <input type="text" class="num_6a" name="num_6b" value="" placeholder="77">
                                                </div>
                                                <div class="num__char num__06_c">
                                                    <input type="text" class="num_6c" name="num_6c" value="" placeholder="1133">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 07 -->
                                        <div class="product__num">
                                            <div class="num num__07">
                                                <img src="img/num/num_07.svg" class="img-fluid" alt="">
                                                <div class="num__char num__07_a">
                                                    <input type="text" class="num_7a" name="num_7a" value="" placeholder="ММ">
                                                </div>
                                                <div class="num__char num__07_b">
                                                    <input type="text" class="num_7b" name="num_7b" value="" placeholder="55">
                                                </div>
                                                <div class="num__char num__07_c">
                                                    <input type="text" class="num_7c" name="num_7c" value="" placeholder="АА">
                                                </div>
                                                <div class="num__char num__07_d">
                                                    <input type="text" class="num_7d" name="num_7d" value="" placeholder="77">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 08 -->
                                        <div class="product__num">
                                            <div class="num num__08">
                                                <img src="img/num/num_08.svg" class="img-fluid" alt="">
                                                <div class="num__char num__08_a">
                                                    <input type="text" class="num_8a" name="num_8a" value="" placeholder="3773">
                                                </div>
                                                <div class="num__char num__08_b">
                                                    <input type="text" class="num_8a" name="num_8b" value="" placeholder="мм">
                                                </div>
                                                <div class="num__char num__08_c">
                                                    <input type="text" class="num_8c" name="num_8c" value="" placeholder="55">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 09 -->
                                        <div class="product__num">
                                            <div class="num num__09">
                                                <img src="img/num/num_09.svg" class="img-fluid" alt="">
                                                <div class="num__char num__09_a">
                                                    <input type="text" class="num_9a" name="num_9a" value="" placeholder="мм976">
                                                </div>
                                                <div class="num__char num__09_b">
                                                    <input type="text" class="num_9b" name="num_9b" value="" placeholder="78">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 10 -->
                                        <div class="product__num">
                                            <div class="num num__10">
                                                <img src="img/num/num_10.svg" class="img-fluid" alt="">
                                                <div class="num__char num__10_a">
                                                    <input type="text" class="num_10a" name="num_10a" value="" placeholder="7976ММ">
                                                </div>
                                                <div class="num__char num__10_b">
                                                    <input type="text" class="num_10b" name="num_10b" value="" placeholder="99">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 11 -->
                                        <div class="product__num">
                                            <div class="num num__11">
                                                <img src="img/num/num_11.svg" class="img-fluid" alt="">
                                                <div class="num__char num__11_a">
                                                    <input type="text" class="num_11a" name="num_11a" value="" placeholder="ММ7976">
                                                </div>
                                                <div class="num__char num__11_b">
                                                    <input type="text" class="num_11b" name="num_11b" value="" placeholder="99">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 12 -->
                                        <div class="product__num">
                                            <div class="num num__12">
                                                <img src="img/num/num_12.svg" class="img-fluid" alt="">
                                                <div class="num__char num__12_a">
                                                    <input type="text" class="num_12a" name="num_12a" value="" placeholder="3773">
                                                </div>
                                                <div class="num__char num__12_b">
                                                    <input type="text" class="num_12b" name="num_12b" value="" placeholder="мм">
                                                </div>
                                                <div class="num__char num__12_c">
                                                    <input type="text" class="num_12c" name="num_12c" value="" placeholder="55">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 13 -->
                                        <div class="product__num">
                                            <div class="num num__13">
                                                <img src="img/num/num_13.svg" class="img-fluid" alt="">
                                                <div class="num__char num__13_a">
                                                    <input type="text" class="num_13a" name="num_13a" value="" placeholder="1133">
                                                </div>
                                                <div class="num__char num__13_b">
                                                    <input type="text" class="num_13a" name="num_13b" value="" placeholder="АА">
                                                </div>
                                                <div class="num__char num__13_c">
                                                    <input type="text" class="num_13c" name="num_13c" value="" placeholder="77">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 14 -->
                                        <div class="product__num">
                                            <div class="num num__14">
                                                <img src="img/num/num_14.svg" class="img-fluid" alt="">
                                                <div class="num__char num__14_a">
                                                    <input type="text" class="num_14a" name="num_14a" value="" placeholder="876">
                                                </div>
                                                <div class="num__char num__14_b">
                                                    <input type="text" class="num_14b" name="num_14b" value="" placeholder="С">
                                                </div>
                                                <div class="num__char num__14_c">
                                                    <input type="text" class="num_14c" name="num_14c" value="" placeholder="D">
                                                </div>
                                                <div cl
                                                <div class="num__char num__14_d">
                                                    <input type="text" class="num_14d" name="num_14d" value="" placeholder="2">
                                                </div>
                                                <div class="num__char num__14_e">
                                                    <input type="text" class="num_14e" name="num_14e" value="" placeholder="99">
                                                </div>
                                            </div>
                                        </div>

                                        <!-- number 15 -->
                                        <div class="product__num">
                                            <div class="num num__15">
                                                <img src="img/num/num_15.svg" class="img-fluid" alt="">
                                                <div class="num__char num__15_a">
                                                    <input type="text" class="num_15a" name="num_14a" value="" placeholder="876">
                                                </div>
                                                <div class="num__char num__15_b">
                                                    <input type="text" class="num_15b" name="num_15b" value="" placeholder="D">
                                                </div>
                                                <div cl
                                                <div class="num__char num__15_c">
                                                    <input type="text" class="num_15c" name="num_15c" value="" placeholder="234">
                                                </div>
                                                <div class="num__char num__15_d">
                                                    <input type="text" class="num_15d" name="num_15d" value="" placeholder="99">
                                                </div>
                                            </div>
                                        </div>

                                        <!-- number 16 -->
                                        <div class="product__num">
                                            <div class="num num__16">
                                                <img src="img/num/num_16.svg" class="img-fluid" alt="">
                                                <div class="num__char num__16_a">
                                                    <input type="text" class="num_16a" name="num_16a" value="" placeholder="D">
                                                </div>
                                                <div class="num__char num__16_b">
                                                    <input type="text" class="num_16b" name="num_16b" value="" placeholder="876">
                                                </div>
                                                <div class="num__char num__16_c">
                                                    <input type="text" class="num_16c" name="num_16c" value="" placeholder="23">
                                                </div>
                                                <div class="num__char num__16_d">
                                                    <input type="text" class="num_16d" name="num_16d" value="" placeholder="77">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 17 -->
                                        <div class="product__num">
                                            <div class="num num__17">
                                                <img src="img/num/num_17.svg" class="img-fluid" alt="">
                                                <div class="num__char num__17_a">
                                                    <input type="text" class="num_17a" name="num_17a" value="" placeholder="м">
                                                </div>
                                                <div class="num__char num__17_b">
                                                    <input type="text" class="num_17b" name="num_17b" value="" placeholder="9768">
                                                </div>
                                                <div class="num__char num__17_c">
                                                    <input type="text" class="num_17c" name="num_17c" value="" placeholder="77">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 18 -->
                                        <div class="product__num">
                                            <div class="num num__18">
                                                <img src="img/num/num_18.svg" class="img-fluid" alt="">
                                                <div class="num__char num__18_a">
                                                    <input type="text" class="num_18a" name="num_18a" value="" placeholder="976">
                                                </div>
                                                <div class="num__char num__18_b">
                                                    <input type="text" class="num_18b" name="num_18b" value="" placeholder="М">
                                                </div>
                                                <div class="num__char num__18_c">
                                                    <input type="text" class="num_18c" name="num_18c" value="" placeholder="77">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 19 -->
                                        <div class="product__num">
                                            <div class="num num__19">
                                                <img src="img/num/num_19.svg" class="img-fluid" alt="">
                                                <div class="num__char num__19_a">
                                                    <input type="text" class="num_19a" name="num_19a" value="" placeholder="1133">
                                                </div>
                                                <div class="num__char num__19_b">
                                                    <input type="text" class="num_19b" name="num_19b" value="" placeholder="А">
                                                </div>
                                                <div class="num__char num__19_c">
                                                    <input type="text" class="num_19c" name="num_19c" value="" placeholder="77">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 20 -->
                                        <div class="product__num">
                                            <div class="num num__20">
                                                <img src="img/num/num_20.svg" class="img-fluid" alt="">
                                                <div class="num__char num__20_a">
                                                    <input type="text" class="num_20a" name="num_20a" value="" placeholder="мм976">
                                                </div>
                                                <div class="num__char num__20_b">
                                                    <input type="text" class="num_20b" name="num_20b" value="" placeholder="78">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 21 -->
                                        <div class="product__num">
                                            <div class="num num__21">
                                                <img src="img/num/num_21.svg" class="img-fluid" alt="">
                                                <div class="num__char num__21_a">
                                                    <input type="text" class="num_21a" name="num_21a" value="" placeholder="ММ">
                                                </div>
                                                <div class="num__char num__21_b">
                                                    <input type="text" class="num_21b" name="num_21b" value="" placeholder="976">
                                                </div>
                                                <div class="num__char num__21_c">
                                                    <input type="text" class="num_21c" name="num_21c" value="" placeholder="78">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 22 -->
                                        <div class="product__num">
                                            <div class="num num__22">
                                                <img src="img/num/num_22.svg" class="img-fluid" alt="">
                                                <div class="num__char num__22_a">
                                                    <input type="text" class="num_22a" name="num_22a" value="" placeholder="133">
                                                </div>
                                                <div class="num__char num__22_b">
                                                    <input type="text" class="num_5a" name="num_22b" value="" placeholder="АА">
                                                </div>
                                                <div class="num__char num__22_c">
                                                    <input type="text" class="num_22c" name="num_22c" value="" placeholder="47">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 23 -->
                                        <div class="product__num">
                                            <div class="num num__20">
                                                <img src="img/num/num_23.svg" class="img-fluid" alt="">
                                                <div class="num__char num__23_a">
                                                    <input type="text" class="num_23a" name="num_23a" value="" placeholder="мм976">
                                                </div>
                                                <div class="num__char num__23_b">
                                                    <input type="text" class="num_23b" name="num_23b" value="" placeholder="78">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 24 -->
                                        <div class="product__num">
                                            <div class="num num__24">
                                                <img src="img/num/num_24.svg" class="img-fluid" alt="">
                                                <div class="num__char num__24_a">
                                                    <input type="text" class="num_24a" name="num_24a" value="" placeholder="ММ">
                                                </div>
                                                <div class="num__char num__24_b">
                                                    <input type="text" class="num_24b" name="num_24b" value="" placeholder="976">
                                                </div>
                                                <div class="num__char num__24_c">
                                                    <input type="text" class="num_24c" name="num_24c" value="" placeholder="78">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                        <!-- number 25 -->
                                        <div class="product__num">
                                            <div class="num num__25">
                                                <img src="img/num/num_25.svg" class="img-fluid" alt="">
                                                <div class="num__char num__25_a">
                                                    <input type="text" class="num_25a" name="num_25a" value="" placeholder="133">
                                                </div>
                                                <div class="num__char num__25_b">
                                                    <input type="text" class="num_5a" name="num_25b" value="" placeholder="АА">
                                                </div>
                                                <div class="num__char num__25_c">
                                                    <input type="text" class="num_25c" name="num_25c" value="" placeholder="47">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- -->

                                    </div>

                                    <div class="product__order">

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
