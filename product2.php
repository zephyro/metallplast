<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page" id="top">

            <!-- Header -->
            <div class="header_mobile">
                <div class="container">
                    <div class="header_mobile__wrap">
                        <a class="header_mobile__phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                        <a class="header_mobile__email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header_mobile__slogan">Производим автомобильные номера с 1991 года</div>
                    <div class="header__row">
                        <div class="header__left">
                            <a class="header__toggle nav_toggle" href="#"></a>
                            <a class="header__logo" href="/">
                                <img src="img/logo.svg" class="img-fluid" alt="">
                            </a>
                            <div class="header__slogan">Производим автомобильные номера с 1991 года</div>
                        </div>
                        <nav class="header__nav">
                            <ul>
                                <li><a href="#">О Компании</a></li>
                                <li><a href="#">Аксессуары</a></li>
                                <li><a href="#">Доставка и оплата</a></li>
                                <li><a href="#">Для производителей номеров</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </nav>
                        <div class="header__right">

                            <div class="header__slogan_tablet">Производим автомобильные номера с 1991 года</div>

                            <ul class="header__contact">
                                <li>
                                    <a class="header__contact_phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                                </li>
                                <li>
                                    <a class="header__contact_email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                                </li>
                            </ul>

                            <div class="header__cart">
                                <span>2</span>
                                <i>
                                    <img src="img/icon__cart.svg" class="img-fluid" alt="">
                                </i>
                                <a href="#" class="header__cart_link cart_toggle"></a>
                                <div class="mini_cart">
                                    <div class="mini_cart__heading">
                                        <strong>Корзина</strong>
                                        <span>В Вашей корзине 2 товара</span>
                                    </div>
                                    <div class="mini_cart__close cart_toggle"></div>
                                    <div class="mini_cart__content">
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>1</strong>
                                                    </div>
                                                    <a href="#">Легковые грузовые и автобусы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image">
                                                    <img src="images/mini_cat__item_01.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>1 200</strong> ₽</li>
                                        </ul>
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>3</strong>
                                                    </div>
                                                    <a href="#">Дорожно-строительная техника, тракторы, прицепы и полуприцепы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image mini_cart__image_small">
                                                    <img src="images/mini_cat__item_02.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>900</strong> ₽</li>
                                        </ul>
                                    </div>
                                    <div class="mini_cart__footer">
                                        <a href="#" class="btn btn_sm btn_yellow">Перейти в корзину</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <ul class="header__breadcrumbs">
                            <li><a href="#">Интернет-магазин</a></li>
                            <li><a href="#">Гражданские</a></li>
                            <li><span>Легковые, грузовые и автобусы</span></li>
                        </ul>

                    </div>

                </div>
            </header>

            <nav class="nav_mobile">
                <span class="nav_mobile__close nav_toggle"></span>
                <div class="nav_mobile__logo">
                    <img src="img/logo.svg" class="img-fluid" alt="">
                </div>
                <ul>
                    <li><a href="#">Интренет магазин</a></li>
                    <li><a href="#">О Компании</a></li>
                    <li><a href="#">Аксессуары</a></li>
                    <li><a href="#">Доставка и оплата</a></li>
                    <li><a href="#">Для производителей номеров</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </nav>
            <div class="nav_mobile__layout nav_toggle"></div>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="main_row">

                        <div class="sidebar">
                            <div class="sidebar__nav">
                                <div class="catalog__filter_content">
                                    <a href="#" class="catalog__type catalog__type_base active">Пластины для изготовления государственных регистрационных знаков</a>
                                    <a href="#" class="catalog__type catalog__type_base">Окрасочное оборудование и оснастка</a>
                                    <a href="#" class="catalog__type catalog__type_base">Прессовое оборудование и оснастка</a>
                                </div>
                            </div>
                        </div>

                        <div class="product">

                            <div class="product__block">

                                <div class="product__heading">
                                    <div class="product__heading_type"><span class="icon_printing"></span></div>
                                    <h1 class="product__heading_name">Пластина. Тип 1, 1Б, 2.  Плоская, с двухзначным кодом региона</h1>
                                </div>

                                <div class="product__row">

                                    <div class="product__main">

                                        <div class="product__image">
                                            <a href="images/product.jpg" class="btn_modal">
                                                <img src="images/product.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>

                                    </div>

                                    <div class="product__order">
                                        <div class="product__order_wrap">
                                            <div class="product__price">
                                                <div class="product__price_label">Цена</div>
                                                <div class="product__price_value"><strong>6000</strong> ₽</div>
                                            </div>
                                            <div class="product__count">
                                                <div class="product__count_label">Количество:</div>
                                                <div class="product__count_input">
                                                    <div class="cart_count">
                                                        <input type="text" class="cart_count__input" value="20">
                                                        <span class="plus">+</span>
                                                        <span class="minus">-</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__order_button">
                                            <button type="submit" class="btn btn_md btn_yellow">Добавить в корзину</button>
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="product__block">
                                <div class="product__block_heading">Описание</div>

                                <p>На первом экране карточки товара располагается интерактивный номерной знак с возможностью ввода необходимых данных для добавления в корзину. Данные (цифры и буквы) должны визуально моделироваться в реальном времени на интерактивном знаке.</p>

                                <p>На первом экране карточки товара должен находиться тип номера.</p>

                                <p>На первом экране карточки товара должна находиться кнопка «добавить в корзину» (для уменьшения времени покупки).</p>

                                <p>В карточке товара должны находиться преимущества заказа в Метапласте. Они должны быть оформлены в виде иконок и подписей к ним. Преимущества (необходимо сохранить последовательность): Лицензия № (иконка лицензии) Лучшая цена (иконка на выбор дизайнеров) Изготовление за 1 день (иконка часы) Быстрая доставка (иконка автомобиля) Соответствует требованиям ГИБДД и ГОСТ (иконка на выбор дизайнеров) Собственное производство (любая иконка, ассоциирующаяся с производством)<br/>
                                Высококачественные материалы (иконка на выбор дизайнеров)</p>

                                <p>В карточке товара должно находиться описание товара (порядка 1000 знаков без пробелов).</p>

                                <p>В карточке товара (ниже описания) должны находиться скан-копии сертификатов и лицензий (порядка 3 штук).</p>

                                <p>В карточке товара должны находиться примеры продукции (галерея изображений).</p>


                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
