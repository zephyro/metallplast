<footer class="footer">
    <div class="container">
        <a href="#top" class="btn_top"></a>
        <div class="footer__row">
            <div class="footer__text">
                © 1992—2019<br/>
                Все права защищены<br/>
                ООО «ПТФ Метапласт»
            </div>
            <nav class="footer__nav">
                <ul>
                    <li><a href="#">Конфиденциальность</a></li>
                    <li><a href="#">Пользовательское соглашение</a></li>
                    <li><a href="#">Помощь</a></li>
                    <li><a href="#">Поиск по сайту</a></li>
                    <li><a href="#">Карта сайта</a></li>
                </ul>
            </nav>
            <div class="footer__dev">
                <a href="#">Разработка сайта</a>
            </div>
        </div>
    </div>
</footer>