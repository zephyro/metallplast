<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="fonts/MuseoSansCyrl/stylesheet.css">

    <!--link rel="stylesheet" href="fonts/Font_MT/Font_MTplast_bigdigitnormal.css"-->

    <link rel="stylesheet" href="fonts/Font_MTplast/Font_MTplast_bigdigitnormal.css">

    <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">

    <link rel="stylesheet" href="css/main.css">

</head>