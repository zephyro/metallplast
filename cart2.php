<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page" id="top">

            <!-- Header -->
            <div class="header_mobile">
                <div class="container">
                    <div class="header_mobile__wrap">
                        <a class="header_mobile__phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                        <a class="header_mobile__email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header_mobile__slogan">Производим автомобильные номера с 1991 года</div>
                    <div class="header__row">
                        <div class="header__left">
                            <a class="header__toggle nav_toggle" href="#"></a>
                            <a class="header__logo" href="/">
                                <img src="img/logo.svg" class="img-fluid" alt="">
                            </a>
                            <div class="header__slogan">Производим автомобильные номера с 1991 года</div>
                        </div>
                        <nav class="header__nav">
                            <ul>
                                <li><a href="#">О Компании</a></li>
                                <li><a href="#">Аксессуары</a></li>
                                <li><a href="#">Доставка и оплата</a></li>
                                <li><a href="#">Для производителей номеров</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </nav>
                        <div class="header__right">

                            <div class="header__slogan_tablet">Производим автомобильные номера с 1991 года</div>

                            <ul class="header__contact">
                                <li>
                                    <a class="header__contact_phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                                </li>
                                <li>
                                    <a class="header__contact_email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                                </li>
                            </ul>

                            <div class="header__cart">
                                <span>2</span>
                                <i>
                                    <img src="img/icon__cart.svg" class="img-fluid" alt="">
                                </i>
                                <a href="#" class="header__cart_link cart_toggle"></a>
                                <div class="mini_cart">
                                    <div class="mini_cart__heading">
                                        <strong>Корзина</strong>
                                        <span>В Вашей корзине 2 товара</span>
                                    </div>
                                    <div class="mini_cart__close cart_toggle"></div>
                                    <div class="mini_cart__content">
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>1</strong>
                                                    </div>
                                                    <a href="#">Легковые грузовые и автобусы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image">
                                                    <img src="images/mini_cat__item_01.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>1 200</strong> <i class="ruble">¤</i></li>
                                        </ul>
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>3</strong>
                                                    </div>
                                                    <a href="#">Дорожно-строительная техника, тракторы, прицепы и полуприцепы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image mini_cart__image_small">
                                                    <img src="images/mini_cat__item_02.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>900</strong> <i class="ruble">¤</i></li>
                                        </ul>
                                    </div>
                                    <div class="mini_cart__footer">
                                        <a href="#" class="btn btn_sm btn_yellow">Перейти в корзину</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <ul class="header__breadcrumbs">
                            <li><a href="#">Интернет-магазин</a></li>
                            <li><span>Корзина</span></li>
                        </ul>

                    </div>

                </div>
            </header>

            <nav class="nav_mobile">
                <span class="nav_mobile__close nav_toggle"></span>
                <div class="nav_mobile__logo">
                    <img src="img/logo.svg" class="img-fluid" alt="">
                </div>
                <ul>
                    <li><a href="#">Интренет магазин</a></li>
                    <li><a href="#">О Компании</a></li>
                    <li><a href="#">Аксессуары</a></li>
                    <li><a href="#">Доставка и оплата</a></li>
                    <li><a href="#">Для производителей номеров</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </nav>
            <div class="nav_mobile__layout nav_toggle"></div>
            <!-- -->

            <section class="main">
                <div class="container">

                    <div class="checkout_heading">
                        <h1>Корзина</h1>
                        <p>В Вашей корзине 2 товара:</p>
                    </div>

                    <div class="checkout">

                        <div class="checkout__block">

                            <div class="cart">
                                <ul class="cart__row cart__row_heading">
                                    <li>Товар</li>
                                    <li>Количество</li>
                                    <li>Документы</li>
                                    <li>Цена</li>
                                    <li>Удалить</li>
                                </ul>

                                <ul class="cart__row">
                                    <li>
                                        <div class="cart__heading">Товар</div>
                                        <div class="cart__source">

                                            <div class="cart__auto">
                                                <div class="cart__auto_icon">тип <strong>1</strong></div>
                                                <div class="cart__auto_text">Легковые, грузовые и автобусы</div>
                                            </div>

                                            <ul class="cart__params">
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1">
                                                        <span>с отверстиями</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1">
                                                        <span>с флагом</span>
                                                    </label>
                                                </li>
                                            </ul>

                                            <div class="cart__number">
                                                <div class="cart__number_wrap">

                                                    <div class="number number__type_1">
                                                        <div class="number__image">
                                                            <img src="img/number.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="chars chars__type_1_1">
                                                            <input type="text" class="num_1a" name="num_1a" value="" placeholder="м090мм">
                                                        </div>
                                                        <div class="chars chars__type_1_2">
                                                            <input type="text" class="num_1b" name="num_1b" value="" placeholder="090">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="cart__btn">

                                                <button class="btn btn_reg" type="button">
                                                    <i><img src="img/icon__edit.svg" class="img-fluid" alt=""></i>
                                                    <span>Редактировать номер</span>
                                                </button>

                                                <button class="btn btn_white btn_del" type="button">
                                                    <span>Удалить из<br/>корзины</span>
                                                </button>

                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Количество</div>
                                        <div class="cart__source">

                                            <ul class="cart__amount">
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1" checked>
                                                        <span>2 шт.</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="check" value="1">
                                                        <span>1 шт.</span>
                                                    </label>
                                                </li>
                                            </ul>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Документы</div>
                                        <div class="cart__source">
                                            <h4>Свидететельство о регистрации ТС или ПТС*</h4>
                                            <ul class="cart__docs">
                                                <li>
                                                    <label class="cart__upload">
                                                        <input type="file" name="file">
                                                        <strong>1 сторона</strong>
                                                        <span>Загрузить</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="cart__upload">
                                                        <input type="file" name="file">
                                                        <strong>2 сторона</strong>
                                                        <span>Загрузить</span>
                                                    </label>
                                                </li>
                                            </ul>
                                            <div class="cart__upload_text">
                                                * Скан или фотография<br/>
                                                Формат файла jpg/JPG/jpeg
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Цена</div>
                                        <div class="cart__source">
                                            <div class="cart__price"><strong>2000</strong> <i class="ruble">¤</i></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__source">
                                            <button class="btn btn_del" type="button">
                                                <span>Удалить из<br/>корзины</span>
                                            </button>
                                        </div>

                                    </li>
                                </ul>


                                <ul class="cart__row">
                                    <li>
                                        <div class="cart__heading">Товар</div>
                                        <div class="cart__source">

                                            <div class="cart__auto">
                                                <div class="cart__auto_icon"><span class="icon_printing"></span></div>
                                                <div class="cart__auto_text">Пластина. Тип 1, 1Б, 2. Плоская, с двухзначным кодом региона</div>
                                            </div>


                                            <div class="cart__number">
                                                <div class="cart__number_wrap">
                                                    <img src="images/number_photo.jpg" class="img-fluid" alt="">
                                                </div>
                                            </div>

                                            <div class="cart__btn cart__btn_single">

                                                <button class="btn btn_white btn_del" type="button">
                                                    <span>Удалить из<br/>корзины</span>
                                                </button>

                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Количество</div>
                                        <div class="cart__source">

                                            <div class="cart_count">
                                                <input type="text" class="cart_count__input" value="20">
                                                <span class="plus">+</span>
                                                <span class="minus">-</span>
                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart_no_docs">
                                            <div class="cart_no_docs__wrap">
                                                <span>Для данного типа товаров загрузка свидететельства о регистрации ТС или ПТС не требуется</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__heading">Цена</div>
                                        <div class="cart__source">
                                            <div class="cart__price"><strong>2000</strong> <i class="ruble">¤</i></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="cart__source">
                                            <button class="btn btn_del" type="button">
                                                <span>Удалить из<br/>корзины</span>
                                            </button>
                                        </div>

                                    </li>
                                </ul>


                            </div>

                        </div>

                        <div class="checkout__consent">
                            <div class="checkout__consent_wrap">
                                <label class="form_checkbox error">
                                    <input type="checkbox" name="consent">
                                    <span>Согласен(а) на обработку персональных данных в соответствии с <a href="#">Пользовательским соглашением</a> ООО «ПТФ Метапласт»</span>
                                </label>
                            </div>
                        </div>

                        <div class="checkout__block">

                            <ul class="purchaser_type">
                                <li>
                                    <label class="form_radio form_radio_md">
                                        <input type="radio" name="purchaser_type" value="private" checked>
                                        <span>Физическое лицо</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form_radio form_radio_md">
                                        <input type="radio" name="purchaser_type" value="corp">
                                        <span>Юридическое лицо</span>
                                    </label>
                                </li>
                            </ul>

                            <div class="checkout__group">

                                <div class="purchaser_tab active" id="private">

                                    <div class="purchaser_private">
                                        <div class="purchaser_private__form">
                                            <div class="form_group">
                                                <input class="form_control error" type="text" name="name" placeholder="ФИО *">
                                            </div>
                                            <div class="form_group">
                                                <input class="form_control error" type="text" name="phone" placeholder="Телефон *">
                                            </div>
                                            <div class="form_group">
                                                <input class="form_control" type="text" name="email" placeholder="Email *">
                                            </div>
                                            <div class="form_group">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="check">
                                                    <span>Я прочитал(а) и согласен(а) с условиями <a href="#">Договора оферты</a></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="purchaser_private__docs">

                                            <h4>Загрузить документ удостоверяющий личность:</h4>
                                            <p class="text-center mb_25 hide-sm">*Формат файла JPG, PNG</p>

                                            <ul>
                                                <li>
                                                    <p>
                                                        Паспорт 1-я страница<br/>
                                                        или Водительское удостоверение сторона 1*
                                                    </p>
                                                    <div class="docs">
                                                        <div class="docs__row">
                                                            <div class="docs__passport">
                                                                <img src="img/doc__xl.svg" class="img-fluid" alt="">
                                                            </div>
                                                            <div class="docs__text">или</div>
                                                            <div class="docs__license">
                                                                <img src="img/doc__sm.svg" class="img-fluid" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <label class="docs__upload">
                                                                <input type="file" value="">
                                                                <span class="docs__upload_text">Загрузить</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <small class="hide-xs-only">*Формат файла JPG, PNG</small>
                                                </li>
                                                <li>
                                                    <p>
                                                        Паспорт страница регистрации<br/>
                                                        или Водительское удостоверение сторона 2*
                                                    </p>
                                                    <div class="docs">
                                                        <div class="docs__row">
                                                            <div class="docs__passport">
                                                                <img src="img/doc__xl.svg" class="img-fluid" alt="">
                                                            </div>
                                                            <div class="docs__text">или</div>
                                                            <div class="docs__license">
                                                                <img src="img/doc__sm.svg" class="img-fluid" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <label class="docs__upload">
                                                                <input type="file" value="">
                                                                <span class="docs__upload_text">Загрузить</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>

                                </div>

                                <div class="purchaser_tab" id="corp">
                                    <div class="purchaser_corp">
                                        <div class="purchaser_corp__left">
                                            <div class="form_group">
                                                <input class="form_control" type="text" name="name" placeholder="Контактное лицо *">
                                            </div>
                                            <div class="form_group">
                                                <input class="form_control" type="text" name="email" placeholder="e-mail *">
                                            </div>
                                            <div class="form_group">
                                                <input class="form_control" type="text" name="phone" placeholder="Телефон *">
                                            </div>
                                        </div>
                                        <div class="purchaser_corp__right">
                                            <div class="form_group">
                                                <input class="form_control" type="text" name="company" placeholder="Наименование юридического лица в соответствии с учр. документами *">
                                            </div>
                                            <div class="form_group">
                                                <label class="docs_file">
                                                    <input type="file" name="docs">
                                                    <div class="docs_file__text">
                                                        <strong>Загрузить карточку Организации</strong>
                                                        <span>Для формирования счета-фактуры, товарных накладных</span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="form_group">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="name" value="">
                                                    <span>Я прочитал(а) и согласен(а) с условиями Договора оферты</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="checkout__group">

                                <ul class="delivery_type">
                                    <li>
                                        <label class="form_radio form_radio_md">
                                            <input type="radio" name="delivery_type" value="pickup">
                                            <span>Самовывоз</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="form_radio form_radio_md">
                                            <input type="radio" name="delivery_type" value="delivery" checked>
                                            <span>Доставка</span>
                                        </label>
                                    </li>
                                </ul>

                                <div class="delivery active" id="delivery">

                                    <h4>Укажите адрес доставки для расчета стоимости:</h4>

                                    <ul class="delivery__form">
                                        <li>
                                            <input class="form_control" type="text" name="city" placeholder="Город…">
                                        </li>
                                        <li>
                                            <input class="form_control" type="text" name="zip" placeholder="Индекс">
                                        </li>
                                        <li>
                                            <input class="form_control" type="text" name="street" placeholder="Улица">
                                        </li>
                                    </ul>

                                    <table class="delivery__table">
                                        <tr>
                                            <th>Способы доставки:</th>
                                            <th>Описание доставки</th>
                                            <th>Цена доставки</th>
                                            <th class="hide-xs-only">Стоимость заказа</th>
                                            <th class="hide-xs-only">Итого</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="form_radio">
                                                    <input type="radio" name="delivery_place" value="1" checked>
                                                    <span>Постомат</span>
                                                </label>
                                            </td>
                                            <td>
                                                <a class="delivery__place" href="#">
                                                    <span>Выберите постомат на карте</span>
                                                    <i><img src="img/icon__place.svg" class="img-fluid" alt="">
                                                    </i>
                                                </a>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><strong>300</strong> ₽</div>
                                                <div class="post_days">5 дней</div>
                                            </td>
                                            <td class="hide-xs-only"></td>
                                            <td class="hide-xs-only"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="form_radio">
                                                    <input type="radio" name="delivery_place" value="1">
                                                    <span>Почта России</span>
                                                </label>
                                            </td>
                                            <td>Доставка по всей России</td>
                                            <td>
                                                <div class="delivery__price"><strong>300</strong> ₽</div>
                                                <div class="post_days">5 дней</div>
                                            </td>
                                            <td class="hide-xs-only"></td>
                                            <td class="hide-xs-only"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="form_radio">
                                                    <input type="radio" name="delivery_place" value="1">
                                                    <span>Курьер</span>
                                                </label>
                                            </td>
                                            <td>Экспресс-доставка по всей России</td>
                                            <td>
                                                <div class="delivery__price"><strong>300</strong> ₽</div>
                                                <div class="post_days">5 дней</div>
                                            </td>
                                            <td class="hide-xs-only"></td>
                                            <td class="hide-xs-only"></td>
                                        </tr>
                                    </table>

                                    <div class="delivery__total">
                                        <div class="delivery__total_price"><span>Итого:</span> 300 ₽</div>
                                        <div class="delivery__total_nds">Цены включают НДС</div>
                                    </div>
                                </div>

                                <div class="delivery" id="pickup">

                                    <div class="delivery__pickup">
                                        <div class="delivery__pickup_title">Адрес, где Вы сможете самостоятельно забрать товар:</div>
                                        <div class="delivery__pickup_text">Москва, Локомотивный проезд …..</div>
                                    </div>

                                    <table class="delivery__table">
                                        <tr>
                                            <th>Способ оплаты:</th>
                                            <th>Стоимость самовывоза</th>
                                            <th>Стоимость заказа</th>
                                            <th>Итого</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="form_radio">
                                                    <input type="radio" name="delivery_place" value="1" checked>
                                                    <span>Наличными или банковской картой при получении</span>
                                                </label>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><strong>300</strong> ₽</div>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><span>2100</span> ₽</div>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><strong>2400</strong> ₽</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="form_radio">
                                                    <input type="radio" name="delivery_place" value="1" checked>
                                                    <span>Банковской картой на сайте</span>
                                                </label>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><strong>300</strong> ₽</div>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><span>2100</span> ₽</div>
                                            </td>
                                            <td>
                                                <div class="delivery__price"><strong>2400</strong> ₽</div>
                                            </td>
                                        </tr>
                                    </table>

                                    <div class="delivery__summary">Цены включают НДС</div>

                                </div>


                            </div>

                        </div>

                        <div class="checkout__accept">
                            <button class="btn btn_yellow btn_md" type="submit"><span>ОФОРМИТЬ ЗАКАЗ И ОПЛАТИТЬ</span></button>
                        </div>

                    </div>


                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
