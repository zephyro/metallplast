<!doctype html>
<html class="no-js" lang="">

<?php include('inc/head.inc.php') ?>

<body>

<div class="page" id="top">

    <!-- Header -->
    <div class="header_mobile">
        <div class="container">
            <div class="header_mobile__wrap">
                <a class="header_mobile__phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                <a class="header_mobile__email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
            </div>
        </div>
    </div>

    <header class="header">
        <div class="container">
            <div class="header_mobile__slogan">Производим автомобильные номера с 1991 года</div>
            <div class="header__row">
                <div class="header__left">
                    <a class="header__toggle nav_toggle" href="#"></a>
                    <a class="header__logo" href="/">
                        <img src="img/logo.svg" class="img-fluid" alt="">
                    </a>
                    <div class="header__slogan">Производим автомобильные номера с 1991 года</div>
                </div>
                <nav class="header__nav">
                    <ul>
                        <li><a href="#">О Компании</a></li>
                        <li><a href="#">Аксессуары</a></li>
                        <li><a href="#">Доставка и оплата</a></li>
                        <li><a href="#">Для производителей номеров</a></li>
                        <li><a href="#">Контакты</a></li>
                    </ul>
                </nav>
                <div class="header__right">

                    <div class="header__slogan_tablet">Производим автомобильные номера с 1991 года</div>

                    <ul class="header__contact">
                        <li>
                            <a class="header__contact_phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                        </li>
                        <li>
                            <a class="header__contact_email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                        </li>
                    </ul>

                    <div class="header__cart">
                        <span>2</span>
                        <i>
                            <img src="img/icon__cart.svg" class="img-fluid" alt="">
                        </i>
                        <a href="#" class="header__cart_link cart_toggle"></a>
                        <div class="mini_cart">
                            <div class="mini_cart__heading">
                                <strong>Корзина</strong>
                                <span>В Вашей корзине 2 товара</span>
                            </div>
                            <div class="mini_cart__close cart_toggle"></div>
                            <div class="mini_cart__content">
                                <ul class="mini_cart__item">
                                    <li>
                                        <div class="mini_cart__name">
                                            <div class="mini_cart__icon">
                                                <span>Тип</span> <strong>1</strong>
                                            </div>
                                            <a href="#">Легковые грузовые и автобусы</a>
                                        </div>
                                        <a href="#" class="mini_cart__image">
                                            <img src="images/mini_cat__item_01.svg" alt="">
                                        </a>
                                    </li>
                                    <li><strong>1 200</strong> <i class="ruble">¤</i></li>
                                </ul>
                                <ul class="mini_cart__item">
                                    <li>
                                        <div class="mini_cart__name">
                                            <div class="mini_cart__icon">
                                                <span>Тип</span> <strong>3</strong>
                                            </div>
                                            <a href="#">Дорожно-строительная техника, тракторы, прицепы и полуприцепы</a>
                                        </div>
                                        <a href="#" class="mini_cart__image mini_cart__image_small">
                                            <img src="images/mini_cat__item_02.svg" alt="">
                                        </a>
                                    </li>
                                    <li><strong>900</strong> <i class="ruble">¤</i></li>
                                </ul>
                            </div>
                            <div class="mini_cart__footer">
                                <a href="#" class="btn btn_sm btn_yellow">Перейти в корзину</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </header>

    <nav class="nav_mobile">
        <span class="nav_mobile__close nav_toggle"></span>
        <div class="nav_mobile__logo">
            <img src="img/logo.svg" class="img-fluid" alt="">
        </div>
        <ul>
            <li><a href="#">Интренет магазин</a></li>
            <li><a href="#">О Компании</a></li>
            <li><a href="#">Аксессуары</a></li>
            <li><a href="#">Доставка и оплата</a></li>
            <li><a href="#">Для производителей номеров</a></li>
            <li><a href="#">Контакты</a></li>
        </ul>
    </nav>
    <div class="nav_mobile__layout nav_toggle"></div>
    <!-- -->

    <div class="content_heading">
        <h1>О Компании ООО «ПТФ Метапласт»</h1>
    </div>


    <section class="main pt_0">
        <div class="container">
            <div class="content">

                <div class="about">
                    <div class="about__image">
                        <img src="images/about__logo.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="about__text">
                        <p>
                            Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию.<br/>
                            Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса. Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию.<br/>
                            Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса. Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию.<br/>
                        </p>
                        <p>
                            Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса. Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию. Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса.
                        </p>

                        <h3>Подзаголовок</h3>
                        <p>
                            Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию.<br/>
                            Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса. Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию.<br/>
                            Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса. Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию.<br/>
                        </p>
                        <p>
                            Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса. Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию. Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса.
                        </p>

                        <h3>Подзаголовок очень очень длинный длинный предлинный</h3>
                        <p>
                            Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию.<br/>
                            Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса. Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию.<br/>
                            Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса. Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию.<br/>
                        </p>
                        <p>
                            Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса. Содружество наших предприятий позволяет предложить Вам широкий спектр производственных услуг и готовой продукцию. Три из них предлагаются Вашему вниманию. С остальными предложениями Вы можете ознакомиться на страницах Интернет-ресурса.
                        </p>
                    </div>
                </div>

                <div class="about_gallery">
                    <div class="about_gallery__heading">Наше производство</div>
                    <div class="about_gallery__wrap">
                        <div class="about_gallery__slider swiper-container">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <div class="swiper-slide">
                                    <a class="about_gallery__item" href="images/gallery__01.jpg" data-fancybox="about">
                                        <img src="images/gallery__01.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a class="about_gallery__item" href="images/gallery__02.jpg" data-fancybox="about">
                                        <img src="images/gallery__02.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a class="about_gallery__item" href="images/gallery__03.jpg" data-fancybox="about">
                                        <img src="images/gallery__03.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a class="about_gallery__item" href="images/gallery__04.jpg" data-fancybox="about">
                                        <img src="images/gallery__04.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>


                                <div class="swiper-slide">
                                    <a class="about_gallery__item" href="images/gallery__01.jpg" data-fancybox="about">
                                        <img src="images/gallery__01.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a class="about_gallery__item" href="images/gallery__02.jpg" data-fancybox="about">
                                        <img src="images/gallery__02.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a class="about_gallery__item" href="images/gallery__03.jpg" data-fancybox="about">
                                        <img src="images/gallery__03.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <a class="about_gallery__item" href="images/gallery__04.jpg" data-fancybox="about">
                                        <img src="images/gallery__04.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <?php include('inc/footer.inc.php') ?>

</div>

<div class="hide">
    <a class="modal_add_open btn_modal" href="#modal_add"></a>
    <div class="modal_add" id="modal_add">
        <div class="modal_add__title">Номер добавлен в корзину.</div>
        <div class="modal_add__text">Для завершения заказа, указания данных номера перейдите в корзину</div>
        <div class="text-center">
            <a href="#" class="btn btn_yellow btn_cart"><span>Перейти в корзину</span></a>
        </div>
    </div>
</div>

<?php include('inc/scripts.inc.php') ?>

</body>
</html>
