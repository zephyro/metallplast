<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page" id="top">

            <!-- Header -->
            <div class="header_mobile">
                <div class="container">
                    <div class="header_mobile__wrap">
                        <a class="header_mobile__phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                        <a class="header_mobile__email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header_mobile__slogan">Производим автомобильные номера с 1991 года</div>
                    <div class="header__row">
                        <div class="header__left">
                            <a class="header__toggle nav_toggle" href="#"></a>
                            <a class="header__logo" href="/">
                                <img src="img/logo.svg" class="img-fluid" alt="">
                            </a>
                            <div class="header__slogan">Производим автомобильные номера с 1991 года</div>
                        </div>
                        <nav class="header__nav">
                            <ul>
                                <li><a href="#">О Компании</a></li>
                                <li><a href="#">Аксессуары</a></li>
                                <li><a href="#">Доставка и оплата</a></li>
                                <li><a href="#">Для производителей номеров</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </nav>
                        <div class="header__right">

                            <div class="header__slogan_tablet">Производим автомобильные номера с 1991 года</div>

                            <ul class="header__contact">
                                <li>
                                    <a class="header__contact_phone" href="tel:+74952251399">+7 (495) 225-13-99</a>
                                </li>
                                <li>
                                    <a class="header__contact_email" href="mailto:info@mtplast.ru">info@mtplast.ru</a>
                                </li>
                            </ul>

                            <div class="header__cart">
                                <span>2</span>
                                <i>
                                    <img src="img/icon__cart.svg" class="img-fluid" alt="">
                                </i>
                                <a href="#" class="header__cart_link cart_toggle"></a>
                                <div class="mini_cart">
                                    <div class="mini_cart__heading">
                                        <strong>Корзина</strong>
                                        <span>В Вашей корзине 2 товара</span>
                                    </div>
                                    <div class="mini_cart__close cart_toggle"></div>
                                    <div class="mini_cart__content">
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>1</strong>
                                                    </div>
                                                    <a href="#">Легковые грузовые и автобусы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image">
                                                    <img src="images/mini_cat__item_01.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>1 200</strong> <i class="ruble">¤</i></li>
                                        </ul>
                                        <ul class="mini_cart__item">
                                            <li>
                                                <div class="mini_cart__name">
                                                    <div class="mini_cart__icon">
                                                        <span>Тип</span> <strong>3</strong>
                                                    </div>
                                                    <a href="#">Дорожно-строительная техника, тракторы, прицепы и полуприцепы</a>
                                                </div>
                                                <a href="#" class="mini_cart__image mini_cart__image_small">
                                                    <img src="images/mini_cat__item_02.svg" alt="">
                                                </a>
                                            </li>
                                            <li><strong>900</strong> <i class="ruble">¤</i></li>
                                        </ul>
                                    </div>
                                    <div class="mini_cart__footer">
                                        <a href="#" class="btn btn_sm btn_yellow">Перейти в корзину</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </header>

            <nav class="nav_mobile">
                <span class="nav_mobile__close nav_toggle"></span>
                <div class="nav_mobile__logo">
                    <img src="img/logo.svg" class="img-fluid" alt="">
                </div>
                <ul>
                    <li><a href="#">Интренет магазин</a></li>
                    <li><a href="#">О Компании</a></li>
                    <li><a href="#">Аксессуары</a></li>
                    <li><a href="#">Доставка и оплата</a></li>
                    <li><a href="#">Для производителей номеров</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </nav>
            <div class="nav_mobile__layout nav_toggle"></div>
            <!-- -->

            <h1 class="page_heading">Заказать дубликаты номеров</h1>

            <div class="cat_nav">
                <div class="container">
                    <div class="cat_nav__slider swiper-container">
                        <div class="swiper-wrapper">

                            <div class="swiper-slide cat_nav__01">
                                <div class="cat_nav__item active">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__01.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Легковые ТС</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__02">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__02.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Такси</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__03">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__03.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Грузовые ТС</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__04">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__04.svg" alt=""">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Автобусы</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__05">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__05.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Мотоциклы</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__06">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__06.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Мопеды</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__07">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__07.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Квадроциклы</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__08">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__08.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Снегоходы</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__09">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__09.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Прицепы</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__10">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__10.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Тракторы</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__11">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__11.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Дор.-строй техника</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__12">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__12.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Ретро</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__12">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__13.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Спортивные</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__12">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__14.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">Военные</div>
                                </div>
                            </div>

                            <div class="swiper-slide cat_nav__12">
                                <div class="cat_nav__item">
                                    <div class="cat_nav__icon">
                                        <div class="cat_nav_wrap">
                                            <img src="img/auto/auto__15.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="cat_nav__title">МВД</div>
                                </div>
                            </div>

                        </div>
                        <!-- Add Arrows -->
                        <div class="cat_nav__next"></div>
                        <div class="cat_nav__prev"></div>
                    </div>
                </div>
            </div>


            <section class="main">
                <div class="container">

                    <div class="catalog">

                        <div class="catalog__filter">
                            <div class="catalog__filter_title"><span>Фильтр по категориям</span></div>
                            <div class="catalog__filter_content">
                                <a href="#" class="catalog__type catalog__type_civil">Гражданские</a>
                                <a href="#" class="catalog__type catalog__type_mil">Военные</a>
                                <a href="#"class="catalog__type catalog__type_diplomatic">Дипломатические</a>
                                <a href="#" class="catalog__type catalog__type_police">МВД</a>
                                <a href="#" class="catalog__type catalog__type_other">Другие</a>
                            </div>
                        </div>

                        <div class="catalog__content">

                            <div class="catalog__row">

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 1А</div>
                                            <div class="goods__heading_name"><span>Легковые, грузовые и автобусы</span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_sm">
                                                    <img src="images/num_02.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p2">
                                                    <span><span>без флага</span></span>
                                                </label>
                                            </div>
                                        </div>    <link rel="stylesheet" href="fonts/HelveticaNeueDeskInterface/styles.css">
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price1" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price1">
                                                    <span><strong>2 шт.</strong><strong>1 600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 1</div>
                                            <div class="goods__heading_name"><span>Легковые, грузовые и автобусы</span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_md">
                                                    <img src="images/num_01.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p2">
                                                    <span><span>без флага</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price2" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price2">
                                                    <span><strong>2 шт.</strong><strong>1000</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 1Б</div>
                                            <div class="goods__heading_name"><span>Такси и маршрутные ТС</span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_md">
                                                    <img src="images/num_03.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price3" checked>
                                                    <span><strong>1 шт.</strong><strong>1600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price3">
                                                    <span><strong>2 шт.</strong><strong>1000</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 2</div>
                                            <div class="goods__heading_name"><span>Прицепы и полуприцепы</span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_md">
                                                    <img src="images/num_12.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p2">
                                                    <span><span>без флага</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price4" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price4">
                                                    <span><strong>2 шт.</strong><strong>1000</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 4</div>
                                            <div class="goods__heading_name"><span>Мотоциклы</span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_xs">
                                                    <img src="images/num_04.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p2">
                                                    <span><span>без флага</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price15" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 4А</div>
                                            <div class="goods__heading_name"><span>Внедорожные мототранспортные средства</span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_xs">
                                                    <img src="images/num_05.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p2">
                                                    <span><span>без флага</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price5" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 4Б</div>
                                            <div class="goods__heading_name"><span>Мопеды</span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_xs">
                                                    <img src="images/num_06.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p2">
                                                    <span><span>без флага</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price6" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 3</div>
                                            <div class="goods__heading_name"><span>Дорожно-строительная техника, тракторы, прицепы и полуприцепы</span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_lg">
                                                    <img src="images/num_06.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p2">
                                                    <span><span>без флага</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price7" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 19</div>
                                            <div class="goods__heading_name"><span>Транзитные номера </span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_md">
                                                    <img src="images/num_07.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p2">
                                                    <span><span>без флага</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price8" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price8">
                                                    <span><strong>2 шт.</strong><strong>1000</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 5</div>
                                            <div class="goods__heading_name"><span>Легковые, грузовые и автобусы </span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_md">
                                                    <img src="images/num_08.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price9" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price9">
                                                    <span><strong>2 шт.</strong><strong>1000</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 6</div>
                                            <div class="goods__heading_name"><span>Прицепы и полуприцепы</span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_md">
                                                    <img src="images/num_08.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price10" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price10">
                                                    <span><strong>2 шт.</strong><strong>1000</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__item">
                                    <div class="goods">
                                        <div class="goods__heading">
                                            <div class="goods__heading_type">тип 6</div>
                                            <div class="goods__heading_name"><span>Дорожно-строительная техника, тракторы, прицепы и полуприцепы</span></div>
                                        </div>
                                        <div class="goods__image">
                                            <div class="goods__image_wrap">
                                                <div class="goods__image_lg">
                                                    <img src="images/num_10.svg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="goods__params">
                                            <div class="goods__params_item">
                                                <label class="form_checkbox_inline">
                                                    <input type="checkbox" name="p1">
                                                    <span><span>с отверстиями для крепления</span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__price">
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price11" checked>
                                                    <span><strong>1 шт.</strong><strong>600</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                            <div class="goods__price_item">
                                                <label class="form_checkbox">
                                                    <input type="radio" name="price11">
                                                    <span><strong>2 шт.</strong><strong>1000</strong> <i class="ruble">¤</i></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="goods__cart">
                                            <button type="button" class="btn btn_shadow">Добавить в корзину</button>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </div>
                </div>


            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <div class="hide">
            <a class="modal_add_open btn_modal" href="#modal_add"></a>
            <div class="modal_add" id="modal_add">
                <div class="modal_add__title">Номер добавлен в корзину.</div>
                <div class="modal_add__text">Для завершения заказа, указания данных номера перейдите в корзину</div>
                <div class="text-center">
                    <a href="#" class="btn btn_yellow btn_cart"><span>Перейти в корзину</span></a>
                </div>
            </div>
        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
